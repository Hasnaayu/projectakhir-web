<?php

namespace App\Imports;

use App\Models\Siswa;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class UsersImport implements ToModel, WithHeadingRow
{

	public function model(array $row)
	{
		return new Siswa([
			'nisn' => $row['nisn'],
			'nama_siswa' => $row['nama_siswa'],
			'tempat_lahir' => $row['tempat_lahir'],
			'tanggal_lahir' => $row['tanggal_lahir'],
			'jenis_kelamin' => $row['jenis_kelamin'],
			'nik' => $row['nik'],
			'agama' => $row['agama'],
			'alamat' => $row['alamat'],
			'rt' => $row['rt'],
			'rw' => $row['rw'],
			'dusun' => $row['dusun'],
			'kelurahan' => $row['kelurahan'],
			'kecamatan' => $row['kecamatan'],
			'kode_pos' => $row['kode_pos'],
			'no_telp' => $row['no_telp'],
			'jenis_tinggal' => $row['jenis_tinggal'],
			'asal_sekolah' => $row['asal_sekolah'],
			'anak_keberapa' => $row['anak_keberapa'],
			'jumlah_saudara_kandung' => $row['jumlah_saudara_kandung'],
			'id_ortu' => $row['id_ortu'],
			'id_kelas' => $row['id_kelas'],
			'id_account' => $row['id_account'],
		]);
	}
	
}
