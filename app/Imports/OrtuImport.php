<?php

namespace App\Imports;

use App\Models\Ortu;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class OrtuImport implements ToModel, WithHeadingRow
{

	public function model(array $row)
	{
		return new Ortu([
			'nama_ayah' => $row['nama_ayah'],
			'nama_ibu' => $row['nama_ibu'],
			'nama_wali' => $row['nama_wali'],
			'tahun_lahir_ayah' => $row['tahun_lahir_ayah'],
			'tahun_lahir_ibu' => $row['tahun_lahir_ibu'],
			'tahun_lahir_wali' => $row['tahun_lahir_wali'],
			'jenjang_pendidikan_ayah' => $row['jenjang_pendidikan_ayah'],
			'jenjang_pendidikan_ibu' => $row['jenjang_pendidikan_ibu'],
			'jenjang_pendidikan_wali' => $row['jenjang_pendidikan_wali'],
			'pekerjaan_ayah' => $row['pekerjaan_ayah'],
			'pekerjaan_ibu' => $row['pekerjaan_ibu'],
			'pekerjaan_wali' => $row['pekerjaan_wali'],
			'penghasilan_ayah' => $row['penghasilan_ayah'],
			'penghasilan_ibu' => $row['penghasilan_ibu'],
			'penghasilan_wali' => $row['penghasilan_wali'],
			'nik_ayah' => $row['nik_ayah'],
			'nik_ibu' => $row['nik_ibu'],
			'nik_wali' => $row['nik_wali'],
			'id_account' => $row['id_account'],
		]);
	}

}
