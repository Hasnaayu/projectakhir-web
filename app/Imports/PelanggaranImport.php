<?php

namespace App\Imports;

use App\Models\Pelanggaran;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class PelanggaranImport implements ToModel, WithHeadingRow
{

	public function model(array $row)
	{
		return new Pelanggaran([
			'tanggal_pelanggaran' => $row['tanggal_pelanggaran'],
			'pelanggaran' => $row['pelanggaran'],
			'tindak_lanjut' => $row['tindak_lanjut'],
			'id_siswa' => $row['id_siswa'],
			'id_gurubk' => $row['id_gurubk'],
		]);
	}

}
