<?php

namespace App\Imports;

use App\Models\Gurubk;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class GurubkImport implements ToModel, WithHeadingRow
{

	public function model(array $row)
	{
		return new Gurubk([
			'nama_gurubk' => $row['nama_gurubk'],
			'tanggal_lahir' => $row['tanggal_lahir'],
			'jenis_kelamin' => $row['jenis_kelamin'],
			'no_telp' => $row['no_telp'],
			'nik' => $row['nik'],
			'alamat' => $row['alamat'],
			'kode_pos' => $row['kode_pos'],
			'id_account' => $row['id_account'],
		]);
	}
}
