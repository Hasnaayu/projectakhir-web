<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use App\Models\User;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data['id_chat'] = $this->id_chat;
        $data['user'] = auth()->user()->id == $this->id_account ? new UserResource(User::find($this->id_accountsecond)) :  new UserResource(User::find($this->id_account)) ;
        $data['created_at'] = $this->created_at;
        $data['messages'] = MessageResource::collection($this->messages);
        return $data;
    }
}
