<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data['id_message'] = $this->id_message;
        $data['content'] = $this->content;
        $data['read'] = $this->read;
        $data['id_account'] = $this->id_account;
        $data['id_chat'] = $this->id_chat;
        $data['created_at'] = $this->created_at;
        return $data;
    }
}
