<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Chatbot;
use App\Models\Bots;
use App\Models\Topik;
use App\Models\Jawabanbot;
use App\Models\pertanyaanbot;
use App\Models\sesichat;

class ChatbotController extends Controller
{
	public function storeresponse(Request $request)
	{
		$chatbot = Chatbot::create([
			'sesichat_id' => $request->sesichat_id,
			'id_pertanyaan' => $request->id_pertanyaan,
		]);

		return response()->json([
			'success' => true,
			'message' => 'Sukses menambahkan data', 
			'data' => $chatbot,
		], 200);
	}

	public function responsesiswa($id)
	{
		$respsiswa = sesichat::with('chatbot.pertanyaan')->firstOrFail('id_sesi', '=', $id);
		$respsiswa = $respsiswa -> toArray();

		$chats = [];
		foreach ($respsiswa['chatbot'] as $key => $value) {
			array_push($chats, $this -> convert_chat_to_bubble($value));
		}

		//dd($respsiswa['chatbot']);
		return response()->json([
			'data' => $chats,
		], 200);
	}

	private function bubble_chat($type, $pesan = null) {
		$bubblechat['type'] = $type;
		$bubblechat['pesan'] = $pesan;
		return $bubblechat;
	}

	private function convert_chat_to_bubble($chatData) {
		$result = [];
		array_push($result, $this->bubble_chat('welcome'));
		array_push($result, $this->bubble_chat('pertanyaan', $chatData['pertanyaan']['pertanyaan']));
		array_push($result, $this->bubble_chat('respon', $chatData['pertanyaan']['jawaban']));
		array_push($result, $this->bubble_chat('tanya_membantu'));

        // Apakah user sudah merespon membantu atau tidak
		if($chatData['is_relevant']){
			array_push($result, $this->bubble_chat('jawab_membantu', ($chatData['is_relevant'] == 1) ? 'Iya' : 'Tidak'));
                    // Apakah user sudah merespon alasan
			//dd($chatData['is_relevan']);
			if($chatData['is_relevant'] == 2){
				array_push($result, $this->bubble_chat('tanya_alasan'));

				if($chatData['alasan_relevan'] == null) {
					array_push($result, $this->bubble_chat('jawab_alasan', $chatData['alasan_relevan']));
					array_push($result, $this->bubble_chat('terima_kasih'));
				}
			}else{
				array_push($result, $this->bubble_chat('terima_kasih'));
			}
		}

		return $result;
	}

	public function listpertanyaan()
	{
		$pertanyaanbot = pertanyaanbot::get();
		return response()->json([
			'data' => $pertanyaanbot->map->only(['id_pertanyaan', 'pertanyaan', 'topik'])
		], 200);
	}

	public function tanyajawab()
	{
		$pertanyaanbot = pertanyaanbot::get();
		return response()->json([
			'data' => $pertanyaanbot->map->only(['pertanyaan', 'jawaban'])
		], 200);
	}


	public function jawabanbot($id)
	{
		$jawabanbot = pertanyaanbot::where('id_pertanyaan', $id)->select('pertanyaan','jawaban')->get();

		return response()->json([
			'data' => $jawabanbot,
		], 200);
	}

	public function postbot(Request $request, $id)
	{
		$jawabanbot = pertanyaanbot::where('id_pertanyaan', $id)->first();

		$bot = Bots::create([
			'id_siswa' => Auth::user()->id,
			'id_pertanyaan' => $jawabanbot->id_pertanyaan,
		]);
		return response()->json([
			'success' => true,
			'message' => 'Sukses menambahkan data', 
			'data' => $bot,
		], 200);
	}

	public function jawabbot()
	{
		$jawabanbot = Jawabanbot::get();

		return response()->json([
			'data' => $jawabanbot->map->only(['id_jawaban', 'jawaban'])		], 200);
	}

	public function topik()
	{
		$topik = Topik::get();

		return response()->json([
			'data' => $topik->map->only(['id_topik', 'topik'])		], 200);
	}

	public function topikbot($id)
	{
		$topikbot = pertanyaanbot::where('id_topik', $id)->get();

		return response()->json([
			'data' => $topikbot->map->only(['pertanyaan', 'jawaban'])		], 200);
	}
}
