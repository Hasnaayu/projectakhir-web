<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class UsersController extends Controller
{
	public function login()
	{
		if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'id_role' => 1]) || Auth::attempt(['email' => request('email'), 'password' => request('password'), 'id_role' => 2]) || Auth::attempt(['email' => request('email'), 'password' => request('password'), 'id_role' => 4])) {
			$user = Auth::user();
			$success['token'] = $user->createToken('appToken')->accessToken;

			return response()->json([
				'success' => true,
				'token' => $success,
				'user' => $user
			]);
			
		} else {
			return response()->json([
				'success' => false,
				'message' => 'Invalid Email or Password',
			], 401);
		}
	}

	public function logout(Request $res)
	{
		if (Auth::user()) {
			$user = Auth::user()->token();
			$user->revoke();

			return response()->json([
				'success' => true,
				'message' => 'Logout successfully'
			]);
		} else {
			return response()->json([
				'success' => false,
				'message' => 'Unable to Logout'
			]);
		}
	}

	public function fcmToken(Request $request){

		$user = User::find(auth()->id());
		$user->update(['fcm_token'=>$request['fcm_token']]);

		return response()->json('fcm updated successfully',200);
	}

	public function index()
    {
        $user = User::where('id_role', '!=', 3)->get();
        return response()->json([
            'data' => $user,
        ], 200);
    }

}
