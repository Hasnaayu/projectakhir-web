<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuruBK;
use App\Models\Panggilanortu;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\Ortu;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PanggilanOrtuController extends Controller
{
	public function createPanggilan(Request $request){
		$siswa = Siswa::with('kelas')->where('id_account', Auth::id())->first();
		$gurubk = Gurubk::where('id_account', Auth::id())->first();
		$idsiswa = Siswa::first();
		$this->validate($request, [
			'tanggal' => 'required',
			'topik' => 'required',
			'isDone' => '',
		]);
		$tanggal_parsed = Carbon::parse($request->tanggal);
		$panggilan = Panggilanortu::create([
			'tanggal' => $tanggal_parsed->format('Y-m-d'),
			'topik' => $request->topik,
			'id_gurubk' => $gurubk->id_gurubk,
			'id_siswa' => $request->id_siswa,
			'nama_siswa' => $idsiswa->nama_siswa,
			'isDone' => $request->isDone,
		]);
		return response()->json([
			'success' => true,
			'message' => 'Sukses menambahkan data', 
			'data' => $panggilan,
		], 200);
	}

	public function panggilanortu()
	{

		$id_ortu = Ortu::where('id_account', Auth::user()->id)->value('id_ortu'); 
		$id_siswa = Siswa::where('id_ortu', $id_ortu)->value('id_siswa');
		$panggilan = Panggilanortu::where('id_siswa', $id_siswa)->get(); 

		return response()->json([
			'data' => $panggilan,
		], 200);
	}

	public function add_to_done(Request $request, $id)
	{
		$panggilan = Panggilanortu::where('id_panggilan', '=', $id)->firstOrFail();
		$panggilan->isDone = true;
		$panggilan->save();

		return response()->json([
			'success' => true,
			'message' => 'Successfully changed status.',
			'data' => $panggilan,
		], 200);
	}

	public function add_to_fail(Request $request, $id)
	{

		$panggilan = Panggilanortu::where('id_panggilan', '=', $id)->firstOrFail();
		$panggilan->isDone = false;
		$panggilan->save();

		return response()->json([
			'success' => true,
			'message' => 'Successfully changed status.',
			'data' => $panggilan,
		], 200);
	}


	public function update(Request $request, $id)
	{
		$siswa = Siswa::with('kelas')->where('id_account', Auth::id())->first();
		$gurubk = Gurubk::where('id_account', Auth::id())->first();
		$idsiswa = Siswa::first();
		$request->validate([
			'tanggal' => 'required',
			'topik' => 'required',
			'isDone' => '',
		]);

		$panggilan = Panggilanortu::where('id_panggilan', '=', $id)->firstOrFail();
		$panggilan->id_gurubk = $gurubk->id_gurubk;
		$panggilan->tanggal = $request->tanggal;
		$panggilan->topik = $request->topik;
		$panggilan->isDone = $request->isDone;
        // dd($request->all());       
		$panggilan->save();

		return response()->json([
			'success' => true,
			'message' => 'Successfully edited.',
			'data' => $panggilan,
		], 200);
	}

	public function delete($id)
	{
		$panggilan = Panggilanortu::where('id_panggilan', $id)->delete();

		return response()->json([
			'success' => true,
			'message' => 'Successfully deleted.',
			'data' => $panggilan,
		], 200);
	}

	public function indexpanggilan($id)
	{

		$panggilan = Panggilanortu::where('id_siswa', $id)->get();

		return response()->json([
			'data' => $panggilan,
		], 200);
	}
}
