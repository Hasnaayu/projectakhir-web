<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\JadwalResource;
use App\Models\GuruBK;
use App\Models\Jadwal;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\Ortu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JadwalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createJadwal(Request $request){
        $siswa = Siswa::with('kelas')->where('id_account', Auth::id())->first();
        $gurubk = Gurubk::where('id_gurubk',$siswa->kelas->id_guruBK)->first();
        $idsiswa = Siswa::where('id_account', Auth::id())->first();
        $this->validate($request, [
            'tanggal' => 'required',
            'topik' => 'required',
            'isConfirmed' => '',
        ]);
        $tanggal_parsed = Carbon::parse($request->tanggal);
        $jadwal = Jadwal::create([
            'tanggal' => $tanggal_parsed->format('Y-m-d'),
            'topik' => $request->topik,
            'id_gurubk' => $gurubk->id_gurubk,
            'id_siswa' => $siswa->id_siswa,
            'nama_siswa' => $idsiswa->nama_siswa,
            'isConfirmed' => $request->isConfirmed,
        ]);
        return response()->json([
         'success' => true,
         'message' => 'Sukses menambahkan data', 
         'data' => $jadwal,
     ], 200);
        return $siswa;
    }

    public function update(Request $request, $id)
    {
        $siswa = Siswa::with('kelas')->where('id_account', Auth::id())->first();
        $gurubk = GuruBK::where('id_gurubk',$siswa->kelas->id_guruBK)->first();
        $request->validate([
         'tanggal' => 'required',
         'topik' => 'required',
         'isConfirmed' => '',
     ]);

        $jadwal = Jadwal::where('id_jadwal', '=', $id)->firstOrFail();
        $jadwal->id_siswa = $siswa->id_siswa;
        $jadwal->id_gurubk = $gurubk->id_gurubk;
        $jadwal->tanggal = $request->tanggal;
        $jadwal->topik = $request->topik;
        $jadwal->isConfirmed = $request->isConfirmed;
        // dd($request->all());       
        $jadwal->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully edited.',
            'data' => $jadwal,
        ], 200);
    }

    public function delete($id)
    {
        $jadwal = Jadwal::where('id_jadwal', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted.',
            'data' => $jadwal,
        ], 200);
    }


    public function getJadwalStudent(){
        $siswa = Siswa::where('user_id', Auth::id())->first();
        $jadwal = Jadwal::where('id_siswa', $siswa->id)->get();
        //return view('siswa', )->with([ 'jadwal' => $jadwal ]);;
    }
    public function getJadwalgurubk(){
        $gurubk = GuruBK::where('user_id', Auth::id())->first();
        $jadwal = Jadwal::where('id_gurubk', $gurubk->id)->get();
        //return view('gurubk')->with([ 'data' => $jadwal ]);
    }
    public function confirm($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->isConfirmed = 1;
        $jadwal->save();
        //return redirect('gurubk');
    }
    public function tolak($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->isConfirmed = 2;
        $jadwal->save();
        //return redirect('gurubk');
    }
    public function editJadwalUlang($id){
        $jadwal = Jadwal::findOrFail($id);
        return response()->json([
            'data' => $jadwal
        ]);
    }
    public function updateJadwalUlang(Request $request, $id){
        $siswa = Siswa::with('kelas')->where('user_id', Auth::id())->first();
        $gurubk = GuruBK::with('user')->where('id',$siswa->kelas->id_gurubk)->first();
        $tanggal_parsed = Carbon::parse($request->tanggal);
        $jadwal = Jadwal::find($id);
        $jadwal->tanggal = $tanggal_parsed->format('Y-m-d');
        $jadwal->jam = $request->jam;
        $jadwal->hari = $tanggal_parsed->translatedFormat('l');
        $jadwal->id_gurubk = $gurubk->id;
        $jadwal->id_siswa = $siswa->id;
        $jadwal->isConfirmed = $request->status;
        $jadwal->save();
        return response()->json([ 'success' => true ]);
    }

    public function index()
    {
        //$jadwal = Jadwal::where('id_siswa', $id)->get();
        $id_siswa = Siswa::where('id_account', Auth::user()->id)->value('id_siswa');
        $jadwal = Jadwal::where('id_siswa', $id_siswa)->get();

        return response()->json([
            'data' => $jadwal,
        ], 200);
    }

    public function jadwalguru()
    {

        $id_gurubk = GuruBK::where('id_account', Auth::user()->id)->value('id_gurubk'); 
        $id_kelas = Kelas::where('id_gurubk', $id_gurubk)->value('id_kelas');
        $id_siswa = Siswa::where('id_kelas', $id_kelas)->value('id_siswa');
        $jadwal = Jadwal::where('id_siswa', $id_siswa)->get(); 

        return response()->json([
            'data' => $jadwal,
        ], 200);
    }

    public function add_to_confirm(Request $request, $id)
    {
        $jadwal = Jadwal::where('id_jadwal', '=', $id)->firstOrFail();
        $jadwal->isConfirmed = true;
        $jadwal->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully changed status.',
            'data' => $jadwal,
        ], 200);
    }

    public function add_to_fail(Request $request, $id)
    {
    
        $jadwal = Jadwal::where('id_jadwal', '=', $id)->firstOrFail();
        $jadwal->isConfirmed = false;
        $jadwal->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully changed status.',
            'data' => $jadwal,
        ], 200);
    }

}
