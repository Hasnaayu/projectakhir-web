<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Ortu;
use App\Imports\OrtuImport;
use App\Exports\OrtuExports;
use Maatwebsite\Excel\Facades\Excel;

class OrtuController extends Controller
{
    public function index()
    {
    	// mengambil data dari table ortu
    	$ortu = DB::table('ortus')->get();
 
    	// mengirim data ortu ke view index
    	return view('layouts.ortu',['ortu' => $ortu]);
    }

    // method untuk menampilkan view form tambah ortu
    public function tambahortu()
    {
        // memanggil view tambah
        return view('layouts.tambahortu');
    }

    public function indexortu()
    {
        $ortu = Ortu::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $ortu,
        ], 200);
    }

    // method untuk insert data ke table ortu
    public function store(Request $request)
    {
        // insert data ke table ortu
        DB::table('ortus')->insert([
            //'id_ortu' => $request->id_ortu,
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
            'nama_wali' => $request->nama_wali,
            'tahun_lahir_ayah' => $request->tahun_lahir_ayah,
            'tahun_lahir_ibu' => $request->tahun_lahir_ibu,
            'tahun_lahir_wali' => $request->tahun_lahir_wali,
            'jenjang_pendidikan_ayah' => $request->jenjang_pendidikan_ayah,
            'jenjang_pendidikan_ibu' => $request->jenjang_pendidikan_ibu,
            'jenjang_pendidikan_wali' => $request->jenjang_pendidikan_wali,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'penghasilan_ayah' => $request->penghasilan_ayah,
            'penghasilan_ibu' => $request->penghasilan_ibu,
            'penghasilan_wali' => $request->penghasilan_wali,
            'nik_ayah' => $request->nik_ayah,
            'nik_ibu' => $request->nik_ibu,
            'nik_wali' => $request->nik_wali,
            'id_account' => $request->id_account,
        ]);
        // alihkan halaman ke halaman ortu
        return redirect('/ortu');
    }

    // method untuk edit data ortu
    public function editortu($id_ortu)
    {
        // mengambil data ortu berdasarkan id yang dipilih
        $ortu = DB::table('ortus')->where('id_ortu',$id_ortu)->get();
        // passing data ortu yang didapat ke view editortu.blade.php
        return view('layouts.editortu',['ortu' => $ortu]);
    }

    // update data ortu
    public function update(Request $request)
    {
        // update data ortu
        DB::table('ortus')->where('id_ortu',$request->id_ortu)->update([
            //'id_ortu' => $request->id_ortu,
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
            'nama_wali' => $request->nama_wali,
            'tahun_lahir_ayah' => $request->tahun_lahir_ayah,
            'tahun_lahir_ibu' => $request->tahun_lahir_ibu,
            'tahun_lahir_wali' => $request->tahun_lahir_wali,
            'jenjang_pendidikan_ayah' => $request->jenjang_pendidikan_ayah,
            'jenjang_pendidikan_ibu' => $request->jenjang_pendidikan_ibu,
            'jenjang_pendidikan_wali' => $request->jenjang_pendidikan_wali,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'penghasilan_ayah' => $request->penghasilan_ayah,
            'penghasilan_ibu' => $request->penghasilan_ibu,
            'penghasilan_wali' => $request->penghasilan_wali,
            'nik_ayah' => $request->nik_ayah,
            'nik_ibu' => $request->nik_ibu,
            'nik_wali' => $request->nik_wali,
            'id_account' => $request->id_account,
        ]);
        // alihkan halaman ke halaman ortu
        return redirect('/ortu');
    }

    // method untuk hapus data ortu
    public function hapusortu($id_ortu)
    {
        // menghapus data ortu berdasarkan id yang dipilih
        DB::table('ortus')->where('id_ortu',$id_ortu)->delete();
            
        // alihkan halaman ke halaman ortu
        return redirect('/ortu');
    }

     public function ortuexports()
    {
       return Excel::download(new OrtuExports, 'ortu.xlsx');
    }

    public function ortuimports(Request $request)
    {
       $request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file');
        $data = Excel::import(new OrtuImport, $path);

       return back()->withStatus('Import in queue, we will send notification after import finished.');
    }
}
