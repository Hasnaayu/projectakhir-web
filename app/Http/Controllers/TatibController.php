<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tatib;

class TatibController extends Controller
{
    public function tatibsatu()
    {

        $tatib = DB::table('tatibsatus')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }

    public function tatibdua()
    {

        $tatib = DB::table('tatibduas')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }

    public function tatibtiga()
    {

        $tatib = DB::table('tatibtigas')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }

    public function tatibempat()
    {

        $tatib = DB::table('tatibempats')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }

    public function tatiblima()
    {

        $tatib = DB::table('tatiblimas')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }

    public function tatibenam()
    {

        $tatib = DB::table('tatibenams')->get();
        //$siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $tatib,
        ], 200);
    }
}
