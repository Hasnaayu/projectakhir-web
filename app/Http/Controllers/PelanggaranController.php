<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pelanggaran;
use App\Models\Siswa;
use App\Models\Ortu;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Kelas;
use DatePeriod;
use App\Imports\PelanggaranImport;
use App\Exports\PelanggaranExports;
use Maatwebsite\Excel\Facades\Excel;

class PelanggaranController extends Controller
{
    private $allBulan = [
        [
            "bulan" => "January",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "February",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "March",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "April",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "May",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "June",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "July",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "August",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "September",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "October",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "November",
            "pelanggaran" => 0
        ],
        [
            "bulan" => "December",
            "pelanggaran" => 0
        ]

    ];
    // private $allTahun = [
    //     [
    //         "tahun" => "2020",
    //         "pelanggaran" => 0
    //     ],
    //     [
    //         "tahun" => "2021",
    //         "pelanggaran" => 0
    //     ],
    // ];
    public function index()
    {
    	// mengambil data dari table pelanggaran
    	$pelanggaran = DB::table('pelanggarans')->get();

    	// mengirim data pelanggaran ke view index
    	return view('layouts.pelanggaran',['pelanggaran' => $pelanggaran]);
    }

    public function pelanggaransiswa()
    {
        $id_siswa = Siswa::where('id_account', Auth::user()->id)->value('id_siswa');
        $pelanggaran = Pelanggaran::where('id_siswa', $id_siswa)->get();
       //$pelanggaran = Pelanggaran::where('id_siswa', '=', auth()->id())->first();

        return response()->json([
            'data' => $pelanggaran,
        ], 200);
    }

    public function pelanggaranortu()
    {

       $id_ortu = Ortu::where('id_account', Auth::user()->id)->value('id_ortu'); //kalo mau pakek langsung $id_ortu
       $id_siswa = Siswa::where('id_ortu', $id_ortu)->value('id_siswa');
       $pelanggaran = Pelanggaran::where('id_siswa', $id_siswa)->get(); 

       return response()->json([
        'data' => $pelanggaran,
    ], 200);
   }

   public function indexpelanggaran($id)
   {

       // $pelanggaran = Pelanggaran::get();
    $pelanggaran = Pelanggaran::where('id_siswa', $id)->get();

    return response()->json([
        'data' => $pelanggaran,
    ], 200);
}

    // method untuk menampilkan view form tambah pelanggaran
public function tambahpelanggaran()
{
        // memanggil view tambah
    return view('layouts.tambahpelanggaran');
}

    // method untuk insert data ke table pelanggaran
public function store(Request $request)
{
        // insert data ke table pelanggaran
    DB::table('pelanggarans')->insert([
        //'id_pelanggaran' => $request->id_pelanggaran,
        'tanggal_pelanggaran' => $request->tanggal_pelanggaran,
        'pelanggaran' => $request->pelanggaran,
        'tindak_lanjut' => $request->tindak_lanjut,
        'id_siswa' => $request->id_siswa,
        'id_guruBK' => $request->id_guruBK,
    ]);
        // alihkan halaman ke halaman pelanggaran
    return redirect('/pelanggaran');
}

public function storepelanggaran(Request $request)
{
    $request->validate(
        [
            'tanggal_pelanggaran' => 'required',
            'pelanggaran' => 'required',
            'tindak_lanjut' => 'required',
        ]
    );

    $pelanggaran = Pelanggaran::create([
        'id_siswa' => $request->id_siswa,
        'id_gurubk' => Auth::user()->id,
        'tanggal_pelanggaran' => $request->tanggal_pelanggaran,
        'pelanggaran' => $request->pelanggaran,
        'tindak_lanjut' => $request->tindak_lanjut,
    ]);

    return response()->json([
     'success' => true,
     'message' => 'Sukses menambahkan data', 
     'data' => $pelanggaran,
 ], 200);

}

    // method untuk edit data pelanggaran
public function editpelanggaran($id_pelanggaran)
{
        // mengambil data pelanggaran berdasarkan id yang dipilih
    $pelanggaran = DB::table('pelanggarans')->where('id_pelanggaran',$id_pelanggaran)->get();
        // passing data pelanggaran yang didapat ke view editpelanggaran.blade.php
    return view('layouts.editpelanggaran',['pelanggaran' => $pelanggaran]);
}

    // update data pelanggaran
public function update(Request $request)
{
        // update data pelanggaran
    DB::table('pelanggarans')->where('id_pelanggaran',$request->id_pelanggaran)->update([
        //'id_pelanggaran' => $request->id_pelanggaran,
        'tanggal_pelanggaran' => $request->tanggal_pelanggaran,
        'pelanggaran' => $request->pelanggaran,
        'tindak_lanjut' => $request->tindak_lanjut,
        'id_siswa' => $request->id_siswa,
        'id_guruBK' => $request->id_guruBK,
    ]);
        // alihkan halaman ke halaman pelanggaran
    return redirect('/pelanggaran');
}

public function updatepelanggaran(Request $request, $id)
{
    $request->validate(
        [
            'tanggal_pelanggaran' => 'required',
            'pelanggaran' => 'required',
            'tindak_lanjut' => 'required',
        ]
    );

        $pelanggaran = Pelanggaran::where('id_pelanggaran', '=', $id)->firstOrFail(); //nama field idnya samakan dengan yg di tabel
        $pelanggaran->id_gurubk = Auth::user()->id;
        $pelanggaran->tanggal_pelanggaran =  $request->tanggal_pelanggaran;
        $pelanggaran->pelanggaran =  $request->pelanggaran;
        $pelanggaran->tindak_lanjut =  $request->tindak_lanjut;
        $pelanggaran->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully edited.', 
            'data' => $pelanggaran,
        ], 200);
    }

    // method untuk hapus data pelanggaran
    public function hapuspelanggaran($id_pelanggaran)
    {
        // menghapus data pelanggaran berdasarkan id yang dipilih
        DB::table('pelanggarans')->where('id_pelanggaran',$id_pelanggaran)->delete();

        // alihkan halaman ke halaman pelanggaran
        return redirect('/pelanggaran');
    }

    // method untuk hapus data pelanggaran
    public function deletepelanggaran($id_pelanggaran)
    {
        // menghapus data pelanggaran berdasarkan id yang dipilih
        $pelanggaran = Pelanggaran::where('id_pelanggaran',$id_pelanggaran)->delete();
        //dd($id_pelanggaran);

        // alihkan halaman ke halaman pelanggaran
        return response()->json([
         'success' => true,
         'message' => 'Sukses menghapus data', 
         'data' => $pelanggaran,
     ], 200);
    }

    public function grafikPelanggaranPerSiswa($id_siswa, $tahun){
        $pelanggaran = Pelanggaran::where('id_siswa', $id_siswa)->whereYear('tanggal_pelanggaran', $tahun)->selectRaw('monthname(tanggal_pelanggaran) bulan, count(*) pelanggaran')
        ->groupBy('bulan')
        ->orderBy('tanggal_pelanggaran', 'ASC')
        ->get();
        $pelanggaran_array = $pelanggaran->toArray();
        $results = array_map(function($elem) use (&$pelanggaran_array){
            foreach($pelanggaran_array as $i => &$a2){
                if($a2['bulan'] == $elem['bulan']){
                    $a2['pelanggaran'] += $elem['pelanggaran'];
                    return;
                }
            }
            return $elem;
        },$this->allBulan);

        $results = array_merge(array_filter($results),$pelanggaran_array);
        usort($results, function ($a, $b) {
            $monthA = date_parse($a['bulan']);
            $monthB = date_parse($b['bulan']);

            return $monthA["month"] - $monthB["month"];
        });

        return response()->json([
         'success' => true,
         'message' => 'Sukses menampilkan data', 
         'data' => $results,
     ], 200);
    }

    public function grafikPelanggaranKelas($id, $tahun){
        $pelanggaran = Pelanggaran::whereHas('siswa', function($query) use ($id) {
            $query->where('id_kelas',$id); 
        })
        ->whereYear('tanggal_pelanggaran', $tahun)
        ->selectRaw("monthname(tanggal_pelanggaran) as bulan, ifnull(count(id_pelanggaran),0) as pelanggaran")
        ->groupBy('bulan')
        ->orderBy('tanggal_pelanggaran', 'ASC')
        ->get();

        $pelanggaran_array = $pelanggaran->toArray();
        $results = array_map(function($elem) use (&$pelanggaran_array){
            foreach($pelanggaran_array as $i => &$a2){
                if($a2['bulan'] == $elem['bulan']){
                    $a2['pelanggaran'] += $elem['pelanggaran'];
                    return;
                }
            }
            return $elem;
        },$this->allBulan);

        $results = array_merge(array_filter($results),$pelanggaran_array);
        usort($results, function ($a, $b) {
            $monthA = date_parse($a['bulan']);
            $monthB = date_parse($b['bulan']);

            return $monthA["month"] - $monthB["month"];
        });
        
        return response()->json([
         'success' => true,
         'message' => 'Sukses menampilkan data', 
         'data' => $results,
     ], 200);
    }

    public function IndexGrafikPelanggaranKelas(){
        $data_kelas = Kelas::get();
        return view('layouts.grafikpelanggarankelas', ['data_kelas' => $data_kelas]);
    }

    public function GrafikPelanggaranSiswa(){
        //$data_siswa = Siswa::get();
        $data_kelas = Kelas::select('id_kelas', 'kelas','jurusan')->get();
        return view('layouts.grafikpelanggaransiswa', ['data_kelas' => $data_kelas]);
    }


    public function grafikPelanggaranForOrtu($tahun){
        $id_ortu = Ortu::where('id_account', Auth::user()->id)->value('id_ortu');
        $id_siswa = Siswa::where('id_ortu', $id_ortu)->value('id_siswa');
        $pelanggaran = Pelanggaran::where('id_siswa', $id_siswa)->whereYear('tanggal_pelanggaran', $tahun)->selectRaw('monthname(tanggal_pelanggaran) bulan, count(*) pelanggaran')
        ->groupBy('bulan')
        ->orderBy('tanggal_pelanggaran', 'ASC')
        ->get();

         foreach ($this->allBulan as $i => $result) {
            if ($pelanggaran->contains('bulan', $result['bulan'])) {
                $this->allBulan[$i]['pelanggaran'] = $pelanggaran->where('bulan', $this->allBulan[$i]['bulan'])->first()->pelanggaran;
            }$this->allBulan[$i]['bulan'] = substr($this->allBulan[$i]['bulan'], 0, 3);
        }

        return response()->json([
         'success' => true,
         'message' => 'Sukses menampilkan data', 
         'data' => $this->allBulan,
     ], 200);
    }


 public function pelanggaranexports()
    {
       return Excel::download(new PelanggaranExports, 'pelanggaran.xlsx');
    }

    public function pelanggaranimports(Request $request)
    {
       $request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file');
        $data = Excel::import(new PelanggaranImport, $path);

       return back()->withStatus('Import in queue, we will send notification after import finished.');
    }
}
