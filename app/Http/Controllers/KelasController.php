<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kelas;
use App\Models\GuruBK;
use App\Models\Siswa;

class KelasController extends Controller
{
	public function index()
	{

		$kelas = Kelas::all();

		return response()->json([
			'data' => $kelas,
		], 200);
	}

	public function indexkelas()
	{
    	// mengambil data dari table kelas
		$kelas = DB::table('kelas')->get();

    	// mengirim data pelanggaran ke view index
		return view('layouts.kelas',['kelas' => $kelas]);
	}

	public function listkelas(){
		$id_guruBK = GuruBK::where('id_account', auth()->id())->first();
		$kelas = Kelas::where('id_guruBK', $id_guruBK->id_gurubk)->get();
		return response()->json([
			'data' => $kelas,
		], 200);
	}

	public function show($id)
	{

		$siswa = Siswa::where('id_kelas', $id)->get();
        return response()->json([
            'data' => $siswa->map->only(['nama_siswa', 'id_siswa'])
        ], 200);
	}

	public function tambahkelas()
	{
        // memanggil view tambah
		return view('layouts.tambahkelas');
	}

	public function store(Request $request)
	{
        // insert data ke table kelas
		DB::table('kelas')->insert([
			'id_kelas' => $request->id_kelas,
			'kelas' => $request->kelas,
			'jurusan' => $request->jurusan,
			'id_guruBK' => $request->id_guruBK,
		]);
        // alihkan halaman ke halaman pelanggaran
		return redirect('/kelas');
	}

	public function editkelas($id_kelas)
	{
        // mengambil data pelanggaran berdasarkan id yang dipilih
		$kelas = DB::table('kelas')->where('id_kelas',$id_kelas)->get();
        // passing data pelanggaran yang didapat ke view editpelanggaran.blade.php
		return view('layouts.editkelas',['kelas' => $kelas]);
	}

	public function update(Request $request)
	{
        // update data pelanggaran
		DB::table('kelas')->where('id_kelas',$request->id_kelas)->update([
			'id_kelas' => $request->id_kelas,
			'kelas' => $request->kelas,
			'jurusan' => $request->jurusan,
			'id_guruBK' => $request->id_guruBK,
		]);
        // alihkan halaman ke halaman pelanggaran
		return redirect('/kelas');
	}

	public function hapuskelas($id_kelas)
	{
        // menghapus data pelanggaran berdasarkan id yang dipilih
		DB::table('kelas')->where('id_kelas',$id_kelas)->delete();

        // alihkan halaman ke halaman pelanggaran
		return redirect('/kelas');
	}
}
