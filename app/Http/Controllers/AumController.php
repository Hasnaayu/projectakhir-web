<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aum;
use App\Models\Siswa;
use Illuminate\Support\Facades\Auth;

class AumController extends Controller
{
    public function indexaum()
    {
        $aum = DB::table('aums')->get();
 
        return response()->json([
            'data' => $aum,
        ], 200);
    }

    public function getaum($id)
    {

        $aum = Aum::where('id_siswa', $id)->get();

        return response()->json([
            'data' => $aum,
        ], 200);
    }

    public function storeaum(Request $request)
    {
        $request->validate(
            [
                'title' => 'required',
                'selected' => 'required',
            ]
        );

        $aum = Aum::create([
            'id_siswa' => Siswa::where('id_account', Auth::user()->id)->value('id_siswa'),
            'title' => $request->title,
            'selected' => $request->selected,
        ]);

        return response()->json([
         'success' => true,
         'message' => 'Sukses menambahkan data', 
         'data' => $aum,
     ], 200);

    }

    public function store(Request $request)
    {
        // insert data ke table siswa
        DB::table('siswas')->insert([
            'id_aum' => $request->id_aum,
            'id_siswa' => $request->id_siswa,
            'title' => $request->title,
            'selected' => $request->selected,
            'created_at' => $request->created_at,
            'updated_at' => $request->updated_at,
        ]);
        // alihkan halaman ke halaman siswa
        return response()->json([
            'data' => $aum,
        ], 200);
    }
}
