<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\MessageResource;
use App\Http\Requests\storeMessageRequest;
use App\Models\Messages;
use App\Models\User;

class MessageController extends Controller
{
    public function store(storeMessageRequest $request)
    {

    	$message = new Messages();
    	$message->content = $request['content'];
    	$message->read = false;
    	$message->id_account = auth()->id();
    	$message->id_chat = (int)$request['id_chat'];
    	$message->save();

    	$chat = $message->chats;

    	$user = User::findOrFail($chat->id_account == auth()->id() ? $chat->id_accountsecond: $chat->id_account);
    	$user->pushNotification(auth()->user()->name.' send you a message',$message->content,$message);

    	return new MessageResource($message);
    }
}
