<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\GuruBK;
use App\Imports\GurubkImport;
use App\Exports\GurubkExports;
use Maatwebsite\Excel\Facades\Excel;

class GurubkController extends Controller
{
    public function index()
    {
        // mengambil data dari table gurubk
        //$gurubk = DB::table('gurubks')->get();
        $gurubk = Gurubk::all();

        // mengirim data gurubk ke view index
        return view('layouts.gurubk',['gurubk' => $gurubk]);
    }

    public function indexgurubk()
    {

        //$gurubk = DB::table('gurubks')->get();
        $gurubk = Gurubk::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $gurubk,
        ], 200);
    }

    // method untuk menampilkan view form tambah gurubk
    public function tambahgurubk()
    {
        // memanggil view tambah
        return view('layouts.tambahgurubk');
    }

    // method untuk insert data ke table gurubk
    public function store(Request $request)
    {
        // insert data ke table gurubk
        DB::table('gurubks')->insert([
            //'id_gurubk' => $request->id_gurubk,
            'nama_gurubk' => $request->nama_gurubk,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_telp' => $request->no_telp,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
            'id_account' => $request->id_account,
        ]);
        // alihkan halaman ke halaman gurubk
        return redirect('/gurubk');
    }

    public function storegurubk(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $foto=null;
        if($request->foto){
            $foto = time().'.'.$request->foto->getClientOriginalExtension();
            $request->foto->move(public_path('images_gurubk'), $foto);
            $gurubk = Gurubk::where('id_gurubk',$request->id_gurubk)->update(['foto'=>$foto]);
            return response()->json([
             'success' => true,
             'message' => 'Sukses menambahkan data', 
             'data' => $gurubk,
         ], 200);
        } else {
            return response()->json([
               'success' => false,
               'message' => 'file foto tidak ditemukan', 
           ], 200);
        }
    }


    // method untuk edit data gurubk
    public function editgurubk($id_gurubk)
    {
        // mengambil data gurubk berdasarkan id yang dipilih
        $gurubk = DB::table('gurubks')->where('id_gurubk',$id_gurubk)->get();
        // passing data gurubk yang didapat ke view editgurubk.blade.php
        return view('layouts.editgurubk',['gurubk' => $gurubk]);
    }

    // update data gurubk
    public function update(Request $request)
    {
        // update data gurubk
        DB::table('gurubks')->where('id_gurubk',$request->id_gurubk)->update([
        //'id_gurubk' => $request->id_gurubk,
            'nama_gurubk' => $request->nama_gurubk,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_telp' => $request->no_telp,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
            'id_account' => $request->id_account,
        ]);
        // alihkan halaman ke halaman gurubk
        return redirect('/gurubk');
    }

    // method untuk hapus data gurubk
    public function hapusgurubk($id_gurubk)
    {
        // menghapus data gurubk berdasarkan id yang dipilih
        DB::table('gurubks')->where('id_gurubk',$id_gurubk)->delete();

        // alihkan halaman ke halaman gurubk
        return redirect('/gurubk');
    }

    public function gurubkexports()
    {
       return Excel::download(new GurubkExports, 'gurubk.xlsx');
    }

    public function gurubkimports(Request $request)
    {
       $request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('import_file');
        $data = Excel::import(new GurubkImport, $path);

       return back()->withStatus('Import in queue, we will send notification after import finished.');
    }
}
