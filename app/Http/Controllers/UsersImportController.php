<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Chat;
use App\Models\Kelas;
use App\Models\Gurubk;
class UsersImportController extends Controller
{
	public function show()
	{
		return view('layouts.siswa');
	}

	public function store(Request $request)
	{
		$request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);
        $array_id_siswa = array();
        $array_id_kelas = array();

        $path = $request->file('import_file');
        $data = Excel::import(new UsersImport, $path);
		$array = Excel::toArray(new UsersImport, $path);
		$data_array =  json_decode(json_encode((array) $array), true)[0];
		foreach($data_array as $siswa){
			$id_guru_bk = Kelas::where('id_kelas', $siswa["id_kelas"])->first()->id_guruBK;
			$id_acc_guru_bk = Gurubk::where('id_gurubk', $id_guru_bk)->first()->id_account;
        	$chat = Chat::create([
	            'id_account' => $siswa["id_account"],
	            'id_accountsecond' => $id_acc_guru_bk
             ]);
		};
		return back()->withStatus('Import in queue, we will send notification after import finished.');
	}
}
