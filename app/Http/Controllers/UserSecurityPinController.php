<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSecurityPin;
use Illuminate\Support\Facades\Auth;


class UserSecurityPinController extends Controller
{
    public function index()
    {
        $user_security_pin = UserSecurityPin::where('user_id', Auth::user()->id)->get();

        return response()->json([
            'data' => $user_security_pin,
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'pin' => 'required|min:4',
                'is_on' => 'required',
            ]
        );
        $user_security_pin = UserSecurityPin::create([
            'user_id' => Auth::user()->id,
            'pin' => $request->pin,
            'is_on' => $request->is_on,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Successfully added.',
            'data' => $user_security_pin,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'pin' => 'required',
            'is_on' => true,
        ]);
        $user_security_pin = UserSecurityPin::where('id', '=', $id)->firstOrFail();

        $user_security_pin->user_id = Auth::user()->id;
        $user_security_pin->pin =  $request->pin;
        $user_security_pin->is_on = $request->is_on;      
        $user_security_pin->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully edited.',
            'data' => $user_security_pin,
        ], 200);
    }

    public function delete($id)
    {
        $user_security_pin = UserSecurityPin::where('id', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted.',
            'data' => $user_security_pin,
        ], 200);
    }
}
