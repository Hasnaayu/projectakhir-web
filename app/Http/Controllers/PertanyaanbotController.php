<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\pertanyaanbots;
use App\Models\sesichat;


class PertanyaanbotController extends Controller
{
    public function tanyabot()
	{
		$tanyabot = DB::table('pertanyaanbots')->get();

		return response()->json([
            'data' => $tanyabot,
        ], 200);
	}

	public function storesesi(Request $request)
    {
        $session = sesichat::firstOrCreate(['id_siswa' => auth()->id()]);

        return response()->json([
         'success' => true,
         'message' => 'Sukses menambahkan data', 
         'data' => $session,
     ], 200);

    }
}
