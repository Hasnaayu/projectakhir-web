<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Bimbingan;
use App\Models\Siswa;

class BimbinganController extends Controller
{
    public function index()
    {
    	// mengambil data dari table bimbingan
    	$bimbingan = DB::table('bimbingans')->get();
 
    	// mengirim data bimbingan ke view index
    	return view('layouts.bimbingan',['bimbingan' => $bimbingan]);
    }

    public function indexbimbingan($id)
    {

        $bimbingan = Bimbingan::where('id_siswa', $id)->get();

        return response()->json([
            'data' => $bimbingan,
        ], 200);
    }

     public function bimbingansiswa()
    {
        $id_siswa = Siswa::where('id_account', Auth::user()->id)->value('id_siswa');
        $bimbingan = Bimbingan::where('id_siswa', $id_siswa)->get();
       //$pelanggaran = Pelanggaran::where('id_siswa', '=', auth()->id())->first();

       return response()->json([
        'data' => $bimbingan,
    ], 200);
   }

    // method untuk menampilkan view form tambah bimbingan
    public function tambahbimbingan()
    {
        // memanggil view tambah
        return view('layouts.tambahbimbingan');
    }

    // method untuk insert data ke table bimbingan
    public function store(Request $request)
    {
        // insert data ke table bimbingan
        DB::table('bimbingans')->insert([
            //'id_bimbingan' => $request->id_bimbingan,
            'tanggal_bimbingan' => $request->tanggal_bimbingan,
            'permasalahan' => $request->permasalahan,
            'solusi' => $request->solusi,
            'id_siswa' => $request->id_siswa,
            'id_guruBK' => $request->id_guruBK,
        ]);
        // alihkan halaman ke halaman bimbingan
        return redirect('/bimbingan');
    }

    public function storebimbingan(Request $request)
    {
        $request->validate(
            [
                'tanggal_bimbingan' => 'required',
                'permasalahan' => 'required',
                'solusi' => 'required',
            ]
        );

        $bimbingan = Bimbingan::create([
            'id_siswa' => $request->id_siswa,
            'id_gurubk' => Auth::user()->id,
            'tanggal_bimbingan' => $request->tanggal_bimbingan,
            'permasalahan' => $request->permasalahan,
            'solusi' => $request->solusi,
        ]);

        return response()->json([
           'success' => true,
           'message' => 'Sukses menambahkan data', 
           'data' => $bimbingan,
       ], 200);

    }

    // method untuk edit data bimbingan
    public function editbimbingan($id_bimbingan)
    {
        // mengambil data bimbingan berdasarkan id yang dipilih
        $bimbingan = DB::table('bimbingans')->where('id_bimbingan',$id_bimbingan)->get();
        // passing data bimbingan yang didapat ke view editbimbingan.blade.php
        return view('layouts.editbimbingan',['bimbingan' => $bimbingan]);
    }

    // update data bimbingan
    public function update(Request $request)
    {
        // update data bimbingan
        DB::table('bimbingans')->where('id_bimbingan',$request->id_bimbingan)->update([
            //'id_bimbingan' => $request->id_bimbingan,
            'tanggal_bimbingan' => $request->tanggal_bimbingan,
            'permasalahan' => $request->permasalahan,
            'solusi' => $request->solusi,
            'id_siswa' => $request->id_siswa,
            'id_guruBK' => $request->id_guruBK,
        ]);
        // alihkan halaman ke halaman bimbingan
        return redirect('/bimbingan');
    }

    public function updatebimbingan(Request $request, $id)
    {

        $request->validate(
            [
                'tanggal_bimbingan' => 'required',
                'permasalahan' => 'required',
                'solusi' => 'required',
            ]
        );

        $bimbingan = Bimbingan::where('id_bimbingan', '=', $id)->firstOrFail();
        // $bimbingan->update(['id_gurubk' => Auth::user()->id]);
        // $bimbingan->update(['permasalahan' => $request->permasalahan]);
        // $bimbingan->update(['solusi' => $request->solusi]);

        $bimbingan->id_gurubk = Auth::user()->id;
        $bimbingan->tanggal_bimbingan =  $request->tanggal_bimbingan;
        $bimbingan->permasalahan =  $request->permasalahan;
        $bimbingan->solusi =  $request->solusi;
        $bimbingan->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully edited.', 
            'data' => $bimbingan,
        ], 200);
    }

    // method untuk hapus data bimbingan
    public function hapusbimbingan($id_bimbingan)
    {
        // menghapus data bimbingan berdasarkan id yang dipilih
        DB::table('bimbingans')->where('id_bimbingan',$id_bimbingan)->delete();
            
        // alihkan halaman ke halaman bimbingan
        return redirect('/bimbingan');
    }

     public function deletebimbingan($id_bimbingan)
    {
        // menghapus data pelanggaran berdasarkan id yang dipilih
        $bimbingan = Bimbingan::where('id_bimbingan',$id_bimbingan)->delete();
        //dd($id_pelanggaran);

        // alihkan halaman ke halaman pelanggaran
        return response()->json([
           'success' => true,
           'message' => 'Sukses menghapus data', 
           'data' => $bimbingan,
       ], 200);
    }

}
