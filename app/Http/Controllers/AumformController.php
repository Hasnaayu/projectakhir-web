<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aumform;
use App\Models\Siswa;
use Illuminate\Support\Facades\Auth;

class AumformController extends Controller
{
	 public function index()
    {
    	// mengambil data dari table pelanggaran
    	$aumform = DB::table('aumforms')->get();

    	// mengirim data pelanggaran ke view index
    	return view('layouts.aumform',['aumforms' => $aumform]);
    }

    public function storeform(Request $request)
    {
        $request->validate(
            [
                'problem' => 'required',
                'desc' => 'required',
            ]
        );

        $aumform = Aumform::create([
            'id_siswa' => Siswa::where('id_account', Auth::user()->id)->value('id_siswa'),
            'problem' => $request->problem,
            'desc' => $request->desc,
        ]);

        return response()->json([
         'success' => true,
         'message' => 'Sukses menambahkan data', 
         'data' => $aumform,
     ], 200);

    }

    public function getaumform($id)
    {

        $aumform = Aumform::where('id_siswa', $id)->get();

        return response()->json([
            'data' => $aumform,
        ], 200);
    }
}
