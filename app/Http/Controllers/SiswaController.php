<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\Chat;
use App\Exports\SiswaExports;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class SiswaController extends Controller
{
    public function index()
    {
        // mengambil data dari table siswa
        $siswa = Siswa::all();

        // mengirim data siswa ke view index
        return view('layouts.siswa',['siswa' => $siswa]);
    }

    public function indexuser()
    {

        //$siswa = DB::table('siswas')->get();
        $siswa = Siswa::where('id_account', '=', auth()->id())->first();

        return response()->json([
            'data' => $siswa,
        ], 200);
    }

    public function indexnama()
    {

        $siswa = Siswa::get();
        return response()->json([
            'data' => $siswa->map->only(['nama_siswa', 'id_kelas'])
        ], 200);
    }

    // method untuk menampilkan view form tambah siswa
    public function tambahsiswa()
    {
        // memanggil view tambah
        return view('layouts.tambahsiswa');
    }

    // method untuk insert data ke table siswa
    public function store(Request $request)
    {
        // insert data ke table siswa
          $request->validate([
            'nisn' => 'required',
            'nama_siswa' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'nik' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required',
            'kecamatan' => 'required',
            'no_telp' => 'required',
            'asal_sekolah' => 'required',
            'id_kelas' => 'required',
            'id_ortu' => 'required',
            'id_account' => 'required'
        ]);

        $siswa = Siswa::create([
            //'id_siswa' => $request->id_siswa,
            'nisn' => $request->nisn,
            'nama_siswa' => $request->nama_siswa,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nik' => $request->nik,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'no_telp' => $request->no_telp,
            'jenis_tinggal' => $request->jenis_tinggal,
            'asal_sekolah' => $request->asal_sekolah,
            'anak_keberapa' => $request->anak_keberapa,
            'jumlah_saudara_kandung' => $request->jumlah_saudara_kandung,
            'id_ortu' => $request->id_ortu,
            'id_kelas' => $request->id_kelas,
            'id_account' => $request->id_account,
        ]);
        $id_gurubk = $siswa->kelas->guruBK->id_account;
        $chat = Chat::create([
            'id_account' => $request->id_account,
            'id_accountsecond' => $id_gurubk
                  ]);
        // alihkan halaman ke halaman siswa
        return redirect('/siswa');
    }

    public function storesiswa(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $foto=null;
        //$siswa = Siswa::where('id_siswa', $request->id_siswa);
        if($request->foto){
            //if($siswa->first()->foto==null){
            $foto = time().'.'.$request->foto->getClientOriginalExtension();
            $request->foto->move(public_path('images_siswa'), $foto);
            $siswa = Siswa::where('id_siswa',$request->id_siswa)->update(['foto'=>$foto]);
            return response()->json([
               'success' => true,
               'message' => 'Sukses menambahkan data', 
               'data' => $siswa,
           ], 200);
            // } else {
            //     $foto = time().'.'.$request->foto->getClientOriginalExtension();
            //     $path = public_path().'/images/';
            //     $oldFoto = $siswa->first()->foto;
            //     $trimmedOldFoto = $path.substr($oldFoto, 28); //buat ambil nama filenya tanpa ada IP/images.. jadi cuma ada nama file, cuma : 169181918.jpg
            //     unlink($trimmedOldFoto);
            //     $request->foto->move(public_path('images'), $foto);
            //     $siswa = $siswa->update(['foto'=>$foto]);
            //     return response()->json([
            //        'success' => true,
            //        'message' => 'Sukses menambahkan data', 
            //        'data' => $siswa,
            //    ], 200);
            // }
        } else {
            return response()->json([
               'success' => false,
               'message' => 'file foto tidak ditemukan', 
           ], 200);
        }
    }
    

    // method untuk edit data siswa
    public function editsiswa($id_siswa)
    {
        // mengambil data siswa berdasarkan id yang dipilih
        $siswa = DB::table('siswas')->where('id_siswa',$id_siswa)->get();
        // passing data siswa yang didapat ke view editsiswa.blade.php
        return view('layouts.editsiswa',['siswa' => $siswa]);
    }

    // update data siswa
    public function update(Request $request)
    {
        // update data siswa
        DB::table('siswas')->where('id_siswa',$request->id_siswa)->update([
            //'id_siswa' => $request->id_siswa,
            'nisn' => $request->nisn,
            'nama_siswa' => $request->nama_siswa,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nik' => $request->nik,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'dusun' => $request->dusun,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'no_telp' => $request->no_telp,
            'jenis_tinggal' => $request->jenis_tinggal,
            'asal_sekolah' => $request->asal_sekolah,
            'anak_keberapa' => $request->anak_keberapa,
            'jumlah_saudara_kandung' => $request->jumlah_saudara_kandung,
            'id_ortu' => $request->id_ortu,
            'id_kelas' => $request->id_kelas,
            'id_account' => $request->id_account,
        ]);
        // alihkan halaman ke halaman siswa
        return redirect('/siswa');
    }

    // method untuk hapus data siswa
    public function hapussiswa($id_siswa)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('siswas')->where('id_siswa',$id_siswa)->delete();

        // alihkan halaman ke halaman pegawai
        return redirect('/siswa');
    }

    public function profile($id_siswa)
    {
        $siswa = DB::table('siswas')->where('id_siswa',$id_siswa)->get();
        return view('layouts.siswa',['siswa' => $siswa]);
    }

    public function siswaperkelas($id_kelas){
        $siswa = Siswa::where('id_kelas', $id_kelas)->select('id_siswa', 'nama_siswa')->get();

        return response()->json([
           'success' => true,
           'data' => $siswa, 
       ], 200); 

    }

    public function siswaexports()
    {
       return Excel::download(new SiswaExports, 'siswa.xlsx');
    }

    public function siswaimports(Request $request)
    {
       $file = $request->file('file');
       $namaFile = $file->getClientOriginalName();
       $file->move('Imports', $namaFile);

       Excel::import(new UsersImport, public_path('/Imports/'.$namaFile));
       return redirect('/siswa');
    }
}