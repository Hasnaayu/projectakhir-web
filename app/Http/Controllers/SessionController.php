<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\SessionResource;
use App\Models\Session;
use App\Events\SessionEvent;

class SessionController extends Controller
{
	public function create(Request $request)
	{
		$session = Session::create(['id_accsiswa'=> auth()->id(), 'id_accgurubk' => 
			$request->friend_id]);
		$modifiedSession = new SessionResource($session);
		broadcast(new SessionEvent($modifiedSession, auth()->id()));

		return response()->json([
			'data' => $modifiedSession,
		], 200);
	}

}
