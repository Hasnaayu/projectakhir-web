<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ChatResource;
use App\Models\Chat;
use App\Models\Messages;

class ChatController extends Controller
{
	public function index()
	{
		$chat = Chat::where('id_account',auth()->user()->id)->orWhere('id_accountsecond',auth()->user()->id)->orderBy('updated_at', 'desc')->get();
		$count = count($chat);
		for ($i = 0; $i < $count; $i++) {
			for ($j = $i + 1; $j < $count; $j++) {
				if (isset($chat[$i]->messages->last()->id) && isset($chat[$j]->messages->last()->id) && $chat[$i]->messages->last()->id < $chat[$j]->messages->last()->id) {
					$temp = $chat[$i];
					$chat[$i] = $chat[$j];
					$chat[$j] = $temp;
				}
			}
		}
		return ChatResource::collection($chat);
	}

	function makeConversationAsReaded(Request $request){
		$request->validate([
			'id_chat'=>'required',
		]);

		$chat = Chat::findOrFail($request['id_chat']);

		foreach ($chat->messages as $message) {
			$message->update(['read'=>true]);
		}

		return response()->json('success',200);
	}

	public function store(Request $request)
	{
		$request->validate([
			'id_account'=>'required',
			'messages'=>'required'
		]);
		$chat = Chat::create([

			'id_account'=>auth()->user()->id,
			'id_accountsecond'=>$request['id_account']
		]);
		
		$message = Messages::create([

			'content'=>$request['messages'],
			'id_account'=>auth()->user()->id,
			'id_chat'=>$chat->id_chat,
			'read'=>false,
		]);
		return new ChatResource($chat);
	}
}
