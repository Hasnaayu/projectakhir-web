<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdmindataController extends Controller
{
	public function index()
	{
    	// mengambil data dari table admin
		$admin = DB::table('admins')->get();

    	// mengirim data admin ke view index
		return view('layouts.admin',['admin' => $admin]);
	}

    // method untuk menampilkan view form tambah admin
	public function tambahadmin()
	{
        // memanggil view tambah
		return view('layouts.tambahadmin');
	}

    // method untuk insert data ke table admin
	public function store(Request $request)
	{
        // insert data ke table admin
		DB::table('admins')->insert([
			'id_admin' => $request->id_admin,
			'nama_admin' => $request->nama_admin,
			'tanggal_lahir' => $request->tanggal_lahir,
			'jenis_kelamin' => $request->jenis_kelamin,
			'no_telp' => $request->no_telp,
			'nik' => $request->nik,
			'alamat' => $request->alamat,
			'kode_pos' => $request->kode_pos,
			'status_admin' => $request->status_admin,
			'id_account' => $request->id_account,
			'foto' => $request->foto,
		]);
        // alihkan halaman ke halaman siswa
		return redirect('/admin');
	}

    // method untuk edit data admin
	public function editadmin($id_admin)
	{
        // mengambil data admin berdasarkan id yang dipilih
		$admin = DB::table('admins')->where('id_admin',$id_admin)->get();
        // passing data admin yang didapat ke view editadmin.blade.php
		return view('layouts.editadmin',['admin' => $admin]);
	}

    // update data admin
	public function update(Request $request)
	{
        // update data admin
		DB::table('admins')->where('id_admin',$request->id_admin)->update([
			'id_admin' => $request->id_admin,
			'nama_admin' => $request->nama_admin,
			'tanggal_lahir' => $request->tanggal_lahir,
			'jenis_kelamin' => $request->jenis_kelamin,
			'no_telp' => $request->no_telp,
			'nik' => $request->nik,
			'alamat' => $request->alamat,
			'kode_pos' => $request->kode_pos,
			'status_admin' => $request->status_admin,
			'id_account' => $request->id_account,
			'foto' => $request->foto,
		]);
       /* if($request->hasFile('foto')) {
        	$request->file('foto')->move('dist/img/', $request->file('foto')->getClientOriginalName());
        	$admin->foto = $request->file('foto')->getClientOriginalName();
        	$admin->save();
        }*/
        // alihkan halaman ke halaman admin
        return redirect('/admin');
    }

    // method untuk hapus data admin
    public function hapusadmin($id_admin)
    {
        // menghapus data admin berdasarkan id yang dipilih
    	DB::table('admins')->where('id_admin',$id_admin)->delete();

        // alihkan halaman ke halaman admin
    	return redirect('/admin');
    }

    public function profile($id_admin)
    {
    	$admin = DB::table('admins')->where('id_admin',$id_admin)->get();
    	return view('layouts.admin',['admin' => $admin]);
    }
}
