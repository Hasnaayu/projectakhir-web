<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //akun guru bk====================================================================================================
    public function akungurubk()
    {
        // mengambil&memfilter data dari table user
        $gurubk = User::where('id_role', '=', 1)->get();
        // mengirim data ke view index
        return view('admin.daftarakungurubk', compact('gurubk'));
    }

    // method untuk menampilkan view form tambah data akun gurubk
    public function tambahakungurubk()
    {
        // memanggil view tambah
        return view('admin.tambahakungurubk');
    }

    // method untuk insert data ke table akun gurubk
    public function storegurubk(Request $request)
    {
        // insert data ke table akun gurubk
        $request->validate([
            'name' => 'required| string| max:255| min:8',
            'email' => 'required| string| max:255| email|unique:users,email,',
            'password' => 'required| string|min:8',
        ]);
        User::insert([
            //'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 1,
        ]);
        // alihkan halaman ke halaman akun gurubk
        return redirect('/daftar/akun/gurubk');
    }    

    // method untuk edit data akun gurubk
    public function editakungurubk($id)
    {
        // mengambil data akun gurubk berdasarkan id yang dipilih
        $gurubk = User::where('id_role', '=', 1)->where('id',$id)->get();
        // passing data akun gurubk yang didapat ke view editakungurubk.blade.php
        return view('admin.editakungurubk',['gurubk' => $gurubk]);
    }

    // update data akun gurubk
    public function updategurubk(Request $request)
    {
        // update data akun gurubk
       $gurubk = User::where('id_role', '=', 1)->where('id',$request->id)->update([
            'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 1,
        ]);
        // alihkan halaman ke halaman akun gurubk
        return redirect('/daftar/akun/gurubk');
    }

    //akun siswa========================================================================================================
    public function akunsiswa()
    {
        $siswa = User::where('id_role', '=', 2)->get();
        return view('admin.daftarakunsiswa', compact('siswa'));
    }

    // method untuk menampilkan view form tambah data akun siswa
    public function tambahakunsiswa()
    {
        // memanggil view tambah
        return view('admin.tambahakunsiswa');
    }

    // method untuk insert data ke table akun siswa
    public function storesiswa(Request $request)
    {
        // insert data ke table akun siswa
        $request->validate([
            'name' => 'required| string| max:255| min:8',
            'email' => 'required| string| max:255| email|unique:users,email,',
            'password' => 'required| string|min:8',
        ]);
        User::insert([
            //'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 2,
        ]);
        
        // alihkan halaman ke halaman akun siswa
        return redirect('/daftar/akun/siswa');
    }    

    // method untuk edit data akun siswa
    public function editakunsiswa($id)
    {
        // mengambil data akun siswa berdasarkan id yang dipilih
        $siswa = User::where('id_role', '=', 2)->where('id',$id)->get();
        // passing data akun admin yang didapat ke view editakunadmin.blade.php
        return view('admin.editakunsiswa',['siswa' => $siswa]);
    }

    // update data akun siswa
    public function updatesiswa(Request $request)
    {
        // update data akun siswa
       $siswa = User::where('id_role', '=', 2)->where('id',$request->id)->update([
            'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 2,
        ]);
        // alihkan halaman ke halaman akun siswa
        return redirect('/daftar/akun/siswa');
    }

    //akun admin=========================================================================================================
     public function akunadmin()
    {
        $admin = User::where('id_role', '=', 3)->get();
        return view('admin.daftarakunadmin', compact('admin'));
    }
    // method untuk menampilkan view form tambah data akun admin
    public function tambahakunadmin()
    {
        // memanggil view tambah
        return view('admin.tambahakunadmin');
    }

    // method untuk insert data ke table akun admin
    public function storeadmin(Request $request)
    {
        // insert data ke table akun admin
        $request->validate([
            'name' => 'required| string| max:255| min:8',
            'email' => 'required| string| max:255| email|unique:users,email,',
            'password' => 'required| string|min:8',
        ]);
        User::insert([
            //'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 3,
        ]);
        // alihkan halaman ke halaman akun admin
        return redirect('/daftar/akun/admin');
    }

    // method untuk edit data akun admin
    public function editakunadmin($id)
    {
        // mengambil data akun admin berdasarkan id yang dipilih
        $admin = User::where('id_role', '=', 3)->where('id',$id)->get();
        // passing data akun admin yang didapat ke view editakunadmin.blade.php
        return view('admin.editakunadmin',['admin' => $admin]);
    }

    // update data akun admin
    public function updateadmin(Request $request)
    {
        // update data akun admin
       $admin = User::where('id_role', '=', 3)->where('id',$request->id)->update([
            'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 3,
        ]);
        // alihkan halaman ke halaman akun admin
        return redirect('/daftar/akun/admin');
    }

    //akun ortu============================================================================================================
     public function akunortu()
    {
        $ortu = User::where('id_role', '=', 4)->get();
        return view('admin.daftarakunortu', compact('ortu'));
    }

    // method untuk menampilkan view form tambah data akun ortu
    public function tambahakunortu()
    {
        // memanggil view tambah
        return view('admin.tambahakunortu');
    }

    // method untuk insert data ke table akun ortu
    public function storeortu(Request $request)
    {
        // insert data ke table akun ortu
        $request->validate([
            'name' => 'required| string| max:255| min:8',
            'email' => 'required| string| max:255| email|unique:users,email,',
            'password' => 'required| string|min:8',
        ]);
        User::insert([
            //'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 4,
        ]);
        // alihkan halaman ke halaman akun ortu
        return redirect('/daftar/akun/ortu');
    }    

    // method untuk edit data akun ortu
    public function editakunortu($id)
    {
        // mengambil data akun ortu berdasarkan id yang dipilih
        $ortu = User::where('id_role', '=', 4)->where('id',$id)->get();
        // passing data akun gurubk yang didapat ke view editakunortu.blade.php
        return view('admin.editakunortu',['ortu' => $ortu]);
    }

    // update data akun ortu
    public function updateortu(Request $request)
    {
        // update data akun ortu
       $ortu = User::where('id_role', '=', 4)->where('id',$request->id)->update([
            'id' => $request->id,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'id_role' => 4,
        ]);
        // alihkan halaman ke halaman akun ortu
        return redirect('/daftar/akun/ortu');
    }
}
