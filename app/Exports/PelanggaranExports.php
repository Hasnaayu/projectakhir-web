<?php
namespace App\Exports;

use App\Models\Pelanggaran;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class PelanggaranExports implements FromCollection, WithHeadingRow
{
	public function collection()
	{   
		return Pelanggaran::all();
	}
}
