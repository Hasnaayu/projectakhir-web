<?php
namespace App\Exports;

use App\Models\Ortu;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class OrtuExports implements FromCollection, WithHeadingRow
{
	public function collection()
	{   
		return Ortu::all();
	}
}
