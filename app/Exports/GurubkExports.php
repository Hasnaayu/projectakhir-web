<?php
namespace App\Exports;

use App\Models\Gurubk;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class GurubkExports implements FromCollection, WithHeadingRow
{
	public function collection()
	{   
		return Gurubk::all();
	}
}
