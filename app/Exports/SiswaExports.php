<?php
namespace App\Exports;

use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class SiswaExports implements FromCollection, WithHeadingRow
{
	public function collection()
	{   
		return Siswa::all();
	}
}
