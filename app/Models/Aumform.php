<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aumform extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_aumform';

    public function siswa(){
        return $this->hasMany('App\Models\Siswa', "id_siswa");
    }
}
