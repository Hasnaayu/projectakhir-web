<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_kelas';

    public function guruBK()
    {
    	return $this->belongsTo(GuruBK::class, "id_guruBK", 'id_gurubk');
    }

     public function siswa(){
        return $this->hasMany('App\Models\Siswa', "id_kelas", 'id_gurubk');
    }
}
