<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'fcm_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'id_role');
    }

    // public function siswa(){
    //     return $this->hasOne('App\Models\Siswa');
    // }

    // public function gurubk(){
    //     return $this->hasOne('App\Models\GuruBK');
    // }
    public function pushNotification($title,$body,$message){

        $token = $this->fcm_token;
        

        if($token == null) return;

        $data['notification']['title']= $title;
        $data['notification']['body']= $body;
        $data['notification']['sound']= true;
        $data['priority']= 'normal';
        $data['data']['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
        $data['data']['message']=$message;
        $data['to'] = $token;
        

        $http = new \GuzzleHttp\Client(['headers'=>[
            'Centent-Type'=>'application/json',
            'Authorization'=>'key=AAAAi707w8E:APA91bFz8OHwNYiK63WvXxgI08bRiXGFgL7xDn4WTTEJWvGbRVeRtcLURlpDi3sORvPH7xPAhG8uhW58cRiG9EYYZQsCLkhG1bSyRri797m4BqM8Deq_A59Z_1uGuu3QtQ8gNyy1kC6t'

        ]]);
        try {
            $response = $http->post('https://fcm.googleapis.com/fcm/send', [ 'json' =>
                    $data
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            // return $e->getCode();
            if ($e->getCode() === 400) {
                return response()->json(['ok'=>'0', 'erro'=> 'Invalid Request.'], $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }
            return response()->json('Something went wrong on the server.', $e->getCode());
        }        

    }

}
