<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pertanyaanbot extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_pertanyaan';
    protected $fillable = ['pertanyaan', 'topik', 'jawaban'];
}
