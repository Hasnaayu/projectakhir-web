<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    use HasFactory;
    //protected $guarded = [];
    protected $fillable = ['sesichat_id', 'is_relevant', 'id_pertanyaan', 'alasan_relevan' ];
	protected $primaryKey = 'id_chatbot';
	//protected $foreignKey = 'sesichat_id';

     public function siswa()
    {
    	return $this->hasMany(Siswa::class, "id_siswa");
    }

    public function pertanyaan()
    {
    	return $this->belongsTo(pertanyaanbot::class, "id_pertanyaan");
    }

    public function sesichat()
    {
    	return $this->belongsTo(sesichat::class, "id_sesi");
    }
}
