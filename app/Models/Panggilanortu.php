<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Panggilanortu extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_panggilan';

    public function guruBK()
    {
    	return $this->hasMany(GuruBK::class, "id_gurubk");
    }
}
