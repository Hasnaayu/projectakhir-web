<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    use HasFactory;

    public function pelanggaran()
    {
    	return $this->hasOne(Pelanggaran::class, "id_pelanggaran");
    }

    public function ortu()
    {
    	return $this->hasMany(Ortu::class, "id_ortu");
    }

    public function guruBK()
    {
    	return $this->hasMany(GuruBK::class, "id_guruBK");
    }
}
