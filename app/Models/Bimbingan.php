<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Bimbingan extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_bimbingan';

    public function siswa()
    {
    	return $this->hasMany(Siswa::class, "id_siswa");
    }

    public function guruBK()
    {
    	return $this->hasMany(GuruBK::class, "id_guruBK");
    }
}
