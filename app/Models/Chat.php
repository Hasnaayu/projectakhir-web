<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = ['id_account', 'id_accountsecond', 'id_chat'];
    protected $primaryKey = 'id_chat';

    // public function siswa()
    // {
    // 	return $this->hasMany(Siswa::class, "id_siswa");
    // }

    // public function guruBK()
    // {
    // 	return $this->hasMany(GuruBK::class, "id_guruBK");
    // }

    public function messages(){
        return $this->hasMany('App\Models\Messages', 'id_chat', 'id_chat');
    }

}
