<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Gurubk extends Model
{
    use HasFactory;
    protected $guarded = [];
     protected $primaryKey = 'id_gurubk';

    public function user()
    {
    	return $this->hasOne(User::class, "id_account");
    }
    //  public function kelas(){
    //     return $this->hasMany('App\Models\Kelas');
    // }

    public function getFotoAttribute()
    {
        if (Auth::check() && Auth::user()->id_role == 3){
            if($this->attributes['foto']!=null){
                return 'http://127.0.0.1:8000/images_gurubk/' . ucfirst($this->attributes['foto']);
            }
            return null;
        }
        else if (Auth::check() && Auth::user()->id_role == 1){
            if($this->attributes['foto']!=null){
                return 'http://10.0.2.2:8000/images_gurubk/' . ucfirst($this->attributes['foto']);
            }
            return null;
        }
    }
}
