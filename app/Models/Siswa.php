<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Siswa extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_siswa';
    protected $hidden = ['id_kelas'];

    public function ortu()
    {
    	return $this->hasOne(Ortu::class, "id_ortu");
    }

    public function pelanggaran()
    {
        return $this->hasMany(pelanggaran::class, "id_siswa", 'id_siswa');
    }

    public function kelas()
    {
    	return $this->belongsTo(Kelas::class, "id_kelas", 'id_kelas');
    }

    public function user()
    {
    	return $this->hasOne(User::class, "id_account");
    }

    public function aumform()
    {
        return $this->belongsTo(Aumform::class, "id_aumform");
    }

    // public function kelas(){
    //     return $this->belongsTo('App\Models\Kelas', 'id_kelas');
    // }

   public function getFotoAttribute()
    {
        if (Auth::check() && Auth::user()->id_role == 3){
            if($this->attributes['foto']!=null){
                return 'http://127.0.0.1:8000/images_siswa/' . ucfirst($this->attributes['foto']);
            }
            return null;
        }
        else if (Auth::check() && Auth::user()->id_role == 2){
            if($this->attributes['foto']!=null){
                return 'http://10.0.2.2:8000/images_siswa/' . ucfirst($this->attributes['foto']);
            }
            return null;
        }
    }
}
