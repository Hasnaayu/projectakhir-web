<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_jadwal';
    protected $dates = ['tanggal'];
    protected $guarded = [];
    public function siswa()
    {
        return $this->belongsTo('App\Models\Siswa', 'id_siswa');
    }
    public function gurubk()
    {
        return $this->belongsTo('App\Models\GuruBK', 'id_gurubk');
    }
}
