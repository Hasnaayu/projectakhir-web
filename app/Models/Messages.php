<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = ['id_chat', 'content', 'id_account', 'read'];
    protected $primaryKey = 'id_message';

    public function chats()
    {
        return $this->belongsTo('App\Models\Chat', 'id_chat', 'id_chat');
    }
}
