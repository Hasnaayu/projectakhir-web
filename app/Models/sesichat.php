<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sesichat extends Model
{
	use HasFactory;
	protected $guarded = [];
	protected $primaryKey = 'id_sesi';

	 public function chatbot()
    {
    	return $this->hasMany(Chatbot::class, "id_chatbot");
    }
}
