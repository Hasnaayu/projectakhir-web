<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ortu extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_ortu';

    public function user()
    {
    	return $this->hasOne(User::class, "id_account");
    }
}
