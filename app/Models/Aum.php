<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aum extends Model
{
    use HasFactory;
      protected $guarded = [];
    protected $primaryKey = 'id_aum';

    public function siswa()
    {
    	return $this->hasMany(Siswa::class, "id_siswa");
    }
}
