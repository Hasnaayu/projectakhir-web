<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Pelanggaran extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $primaryKey = 'id_pelanggaran';

    public function siswa()
    {
    	return $this->belongsTo(Siswa::class, "id_siswa", "id_siswa");
    }

    public function guruBK()
    {
    	return $this->hasMany(GuruBK::class, "id_guruBK");
    }
}
