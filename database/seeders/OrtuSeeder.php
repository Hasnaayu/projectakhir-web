<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ortu;

class OrtuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ortu::create([
               'nama_ayah' => 'Santo',
               'nama_ibu' => 'Santi',
               'nama_wali' => ' ',
               'tahun_lahir_ayah' => '1975',
               'tahun_lahir_ibu' => '1977',
               'jenjang_pendidikan_ayah' => 'S1 Manajemen',
               'jenjang_pendidikan_ibu' => 'S1 Manajemen',
               'jenjang_pendidikan_wali' => ' ',
               'pekerjaan_ayah' => 'Pengusaha',
               'pekerjaan_ibu' => 'Pengusaha',
               'pekerjaan_wali' => ' ',
               'penghasilan_ayah' => '2.000.000',
               'penghasilan_ibu' => '2.000.000',
               'penghasilan_wali' => ' ',
               'nik_ayah' => '12345679098',
               'nik_ibu' => '987654322112',
               'id_account' => '3',
        ]);
    }
}
