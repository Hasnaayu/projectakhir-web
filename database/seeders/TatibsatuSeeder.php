<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatibsatu;

class TatibsatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tatibsatu::create([
    		'isi_tatib' => '1. Bel masuk sekolah berbunyi pukul 07.00 WIB'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' => '2. Peserta didik hadir di sekolah paling lambat 10 menit sebelum bel masuk sekolah berbunyi'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' => '3. Peserta didik dinyatakan terlambat, jika datang ke sekolah setelah bel masuk berbunyi'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' => '4. Peserta didik yang datang terlambat, wajib melapor pada guru piket'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' => '5. Peserta didik tidak boleh meninggalkan sekolah selama kegiatan pembelajaran berlangsung maupun selama jam istirahat, sebelum mendapatkan izin dari guru kelas dan guru piket'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' => '6. Peserta didik yang tidak masuk sekolah karena sakit atau keperluan penting lain, wajib memberikan informasi/surat tertulis dari orang tua/wali'
    	]);

    	Tatibsatu::create([
    		'isi_tatib' =>'7. Peserta didik yang tidak masuk lebih dari tiga hari karena sakit wajib menunjukkan Surat Keterangan Dokter'
    	]);
    }
}
