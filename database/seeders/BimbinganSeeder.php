<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Bimbingan;

class BimbinganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bimbingan::create([
               'tanggal_bimbingan' => '2021/02/11',
               'permasalahan' => 'Konsultasi rencana setelah lulus',
               'solusi' => 'Saran kerja dan saran lanjut kuliah',
               'id_siswa' => '1',
               'id_guruBK' => '5',
        ]);
    }
}
