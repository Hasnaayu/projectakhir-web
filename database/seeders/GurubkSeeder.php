<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Gurubk;

class GurubkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Gurubk::create([
               'nama_gurubk' => 'Lulus Aji Cahyono',
               'tanggal_lahir' => '1988/03/19',
               'jenis_kelamin' => 'L',
               'no_telp' => '081259461164',
               'nik' => '3520051903880000',
               'alamat' => 'Kel. Rejosari 5/2, Ke. Kawedanan',
               'kode_pos' => '63382',
               'id_account' => '4',
        ]);
    }
}
