<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Laporan;

class LaporanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Laporan::create([
               'id_pelanggaran' => '1',
               'id_guruBK' => '1',
               'id_ortu' => '1',
        ]);
    }
}
