<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Messages;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Messages::create([
               'content' => 'Selamat datang di BiKos. Anda dapat melakukan bimbingan dengan guru BK di sini. Silakan sampaikan permasalahan Anda. Tunggu beberapa saat, guru BK akan menjawab permasalahan Anda.',
               'read' => '1',
               'id_account' => '4',
               'id_chat' => '1',
        ]);
    }
}
