<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
       'name' => 'Hasna Ayu',
       'email' => 'hasnaayu@gmail.com',
       'password' => bcrypt('hasnaayu'),
       'id_role' => 3
     ]);

      User::create([
       'name' => 'Zefanya',
       'email' => 'zefanya@gmail.com',
       'password' => bcrypt('zefanyaa'),
       'id_role' => 2
     ]);

      User::create([
       'name' => 'Santi',
       'email' => 'santi@gmail.com',
       'password' => bcrypt('santisan'),
       'id_role' => 4
     ]);

      User::create([
       'name' => 'Lulus Aji',
       'email' => 'lulusaji@gmail.com',
       'password' => bcrypt('lulusaji'),
       'id_role' => 1
     ]);

      User::create([
       'name' => 'Yayuk Sumiati',
       'email' => 'yayuksumiati@gmail.com',
       'password' => bcrypt('yayuksumiati'),
       'id_role' => 1
     ]);

      User::create([
       'name' => 'Barianto',
       'email' => 'barianto@gmail.com',
       'password' => bcrypt('barianto'),
       'id_role' => 1
     ]);
    }
  }
