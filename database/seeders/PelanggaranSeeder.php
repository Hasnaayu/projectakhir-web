<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pelanggaran;

class PelanggaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelanggaran::create([
               'tanggal_pelanggaran' => '2021/02/11',
               'pelanggaran' => 'Tidak mengikuti pelajaran',
               'tindak_lanjut' => 'Tindakan orang tua',
               'id_siswa' => '1',
               'id_guruBK' => '5',
        ]);
    }
}
