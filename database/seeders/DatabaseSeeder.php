<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
		$this->call(SiswaSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(BimbinganSeeder::class);
        $this->call(GurubkSeeder::class);
        $this->call(OrtuSeeder::class);
        $this->call(KelasSeeder::class);
        $this->call(LaporanSeeder::class);
        $this->call(AumSeeder::class);
        $this->call(TatibsatuSeeder::class);
        $this->call(TatibduaSeeder::class);
        $this->call(TatibtigaSeeder::class);
        $this->call(TatibempatSeeder::class);
        $this->call(TatiblimaSeeder::class);
        $this->call(TatibenamSeeder::class);
        $this->call(ChatSeeder::class);
        $this->call(ChatbotSeeder::class);
        $this->call(PelanggaranSeeder::class);
		$this->call(UserSeeder::class);
        $this->call(PertanyaanbotSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(SessionSeeder::class);
    }
}
