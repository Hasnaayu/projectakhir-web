<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatiblima;

class TatiblimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tatiblima::create([
    		'isi_tatib' => '1. Peserta didik tidak pernah meninggalkan kegiatan pembelajaran dalam satu tahun pelajaran'
    	]);

    	Tatiblima::create([
    		'isi_tatib' => '2. Peserta didik memiliki nilai karakter terbaik di sekolah berdasarkan hasil observasi guru, wali kelas, dan guru BK'
    	]);

    	Tatiblima::create([
    		'isi_tatib' => '3. Peserta didik berprestasi dalam bidang akademik atau non-akademik'
    	]);

    	Tatiblima::create([
    		'isi_tatib' => '4. Bentuk dan besar (nilai) penghargaan akan ditentukan sesuai kebijakan Kepala Sekolah'
    	]);
    }
}
