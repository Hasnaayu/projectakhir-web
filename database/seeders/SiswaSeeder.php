<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Siswa;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Siswa::create([
       'nama_siswa' => 'Zefanya User',
       'nisn' => '020400',
       'jenis_kelamin' => 'P',
       'tempat_lahir' => 'Magetan',
       'tanggal_lahir' => '2000/04/02',
       'nik' => '128618265261',
       'agama' => 'Islam',
       'alamat' => 'Jalan Taman Indah 1, Madiun',
       'rt' => '5',
       'rw' => '2',
       'dusun' => '-',
       'kelurahan' => 'Rejosari',
       'kecamatan' => 'kawedanan',
       'kode_pos' => '63382',
       'no_telp' => '08222324255',
       'jenis_tinggal' => 'Bersama Orang Tua',
       'asal_sekolah' => 'SMPN 1 Madiun',
       'anak_keberapa' => '3',
       'jumlah_saudara_kandung' => '3',
       'id_kelas' => '1',
       'id_ortu' => '1',
       'id_account' => '2',
     ]);
    }
  }
