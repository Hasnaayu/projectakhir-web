<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatibenam;

class TatibenamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tatibenam::create([
    		'isi_tatib' => '1. Hal-hal yang belum tercantum dalam Tata Tertib Sekolah ini, akan ditentukan sesuai kebijakan sekolah'
    	]);

    	Tatibenam::create([
    		'isi_tatib' => '2. Tata Tertib Sekolah ini berlaku sejak tanggal ditetapkan'
    	]);
    }
}
