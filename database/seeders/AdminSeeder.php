<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
               'nama_admin' => 'Rossie',
               'tanggal_lahir' => '1977/02/02',
               'jenis_kelamin' => 'Perempuan',
               'no_telp' => '082226551451',
               'nik' => '122637513866',
               'alamat' => 'Jalan Taman 99, Madiun',
               'kode_pos' => '63381',
               'status_admin' => 'Aktif',
               'id_account' => '3',
        ]);
    }
}
