<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
              'id_role' => 1,
              'nama_role' => 'gurubk'
        ]);

        Role::create([
              'id_role' => 2,
              'nama_role' => 'siswa'
        ]);

        Role::create([
              'id_role' => 3,
              'nama_role' => 'admin'
        ]);

        Role::create([
              'id_role' => 4,
              'nama_role' => 'ortu'
        ]);
    }
}
