<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatibdua;

class TatibduaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tatibdua::create([
    		'isi_tatib' => '1. Peserta didik wajib menjunjung tinggi dan melaksanakan TRI SATYA IWP PSM'
    	]);

    	Tatibdua::create([
    		'isi_tatib' => '2. Peserta didik wajib menjaga nama baik sekolah'
    	]);

    	Tatibdua::create([
    		'isi_tatib' => '3. Peserta didik wajib menghormati dan taat pada Kepala Sekolah, Guru, dan Staf Tata Usaha'
    	]);

    	Tatibdua::create([
    		'isi_tatib' => '4. Peserta didik ikut bertanggung jawab atas terselenggaranya kebersihan, keindahan, kelestarian lingkungan, dan keamanan serta kelancaran kegiatan belajar mengajar'
    	]);

    	Tatibdua::create([
    		'isi_tatib' => '5. Peserta didik wajib menumbuhkan dan memelihara rasa kekeluargaan sesama warga sekolah'
    	]);

    	Tatibdua::create([
    		'isi_tatib' => '6. Peserta didik wajib memakai seragam dengan atribut lengkap'
    	]);

    	Tatibdua::create([
    		'isi_tatib' =>'7. Peserta didik wajib berpenampilan rapi, tidak bertato, serta tidak memakai perhiasan dan kosmetik yang berlebihan'
    	]);

    	Tatibdua::create([
    		'isi_tatib' =>'8. Peserta didik wajib mengikuti kegiatan yang diselenggarakan sekolah'
    	]);

    	Tatibdua::create([
    		'isi_tatib' =>'9. Peserta didik wajib ikut serta menjaga dan memelihara sarana prasarana dan inventaris kelas yang ada di sekolah'
    	]);

    	Tatibdua::create([
    		'isi_tatib' =>'10. Peserta didik wajib menjaga ketertiban dan kebersihan kelas serta lingkungan sekolah'
    	]);
    }
}
