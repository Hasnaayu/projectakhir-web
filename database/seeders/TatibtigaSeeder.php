<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatibtiga;

class TatibtigaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Tatibtiga::create([
    		'isi_tatib' => '1. Tidak masuk sekolah tanpa memberikan informasi/surat tertulis dari orang tua/wali'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' => '2. Meninggalkan kelas/bengkel/laboratorium selama kegiatan pembelajaran berlangsung tanpa izin guru kelas'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' => '3. Meninggalkan sekolah selama kegiatan pembelajaran berlangsung maupun selama jam istirahat tanpa izin guru kelas dan guru piket'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' => '4. Tidak memakai seragam selama kegiatan pembelajaran'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' => '5. Memiliki rambut panjang (khusus untuk putra), dicat warna atau dimodel garis'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' => '6. Bertato, memakai anting, kalung, gelang, cincin, dan aksesoris lain yang tidak sesuai dengan dunia pendidikan'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'7. Memakai perhiasan dan kosmetik berlebihan (untuk putri)'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'8. Berada di dalam kelas atau tempat parkir selama jam istirahat'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'9. Menerima tamu tanpa seizin guru piket'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'10. Memakai seragam sekolah di warung, bar, diskotik, karaoke, play station, atau tempat-tempat lain yang tidak berhubungan dengan dunia pendidikan'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'11. Membawa atau menyimpan rokok, minuman keras, narkoba, senjata tajam, buku/media yang berbau SARA, berita bohong (hoax), ujaran kebencian maupun pornografi/pornoaksi serta benda-benda lain yang tidak ada hubungannya dengan dunia pendidikan'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'12. Mengambil barang milik orang lain tanpa izin yang bersangkutan'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'13. Melakukan tindakan yang mengakibatkan kerusakan fasilitas sekolah maupun perorangan'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'14. Membentuk organisasi selain OSIS tanpa izin Kepala Sekolah'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'15. Berkelahi baik secara perorangan atau kelompok'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'16. Membawa senjata api atau sejenisnya'
    	]);

    	Tatibtiga::create([
    		'isi_tatib' =>'17. Menikah atau hamil selama menjadi peserta didik SMK PSM 1 Kawedanan'
    	]);
    }
}
