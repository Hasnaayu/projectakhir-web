<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tatibempat;

class TatibempatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tatibempat::create([
    		'isi_tatib' => '1. Peserta didik yang tidak memakai seragam sesuai ketentuan diminta pulang untuk mengganti pakaiannya'
    	]);

    	Tatibempat::create([
    		'isi_tatib' => '2. Peserta didik yang tidak memakai atribut dengan lengkap diminta untuk melengkapinya'
    	]);

    	Tatibempat::create([
    		'isi_tatib' => '3. Peserta didik (laki-laki) yang memiliki rambut panjang, dicat warna atau dimodel garis akan dirapikan oleh guru piket'
    	]);

    	Tatibempat::create([
    		'isi_tatib' => '4. Peserta didik yang memakai anting, kalung, gelang, cincin, dan aksesoris lain yang tidak sesuai dengan dunia pendidikan akan diambil aksesoris yang dipakainya dan tidak dikembalikan'
    	]);

    	Tatibempat::create([
    		'isi_tatib' => '5. Pemberian teguran secara lisan dilakukan jika peserta didik melakukan pelanggaran ringan pada tata tertib yang berlaku'
    	]);

    	Tatibempat::create([
    		'isi_tatib' => '6. Peringatan tertulis diberikan pada peserta didik yang telah menerima lebih dari tiga kali teguran secara lisan dalam satu bulan'
    	]);

    	Tatibempat::create([
    		'isi_tatib' =>'7. Sanksi membersihkan lingkungan sekolah diberikan apabila peserta didik datang terlambat lebih dari 10 menit dan meninggalkan sekolah tanpa izin pada saat jam pembelajaran berlangsung'
    	]);

    	Tatibempat::create([
    		'isi_tatib' =>'8. Sanksi pemanggilan orang tua/wali dilakukan apabila peserta didik datang terlambat, meninggalkan sekolah tanpa izin, atau pulang sebelum waktunya tanpa izin lebih dari tiga kali dalam satu bulan, serta melakukan pelanggaran berat (melanggar salah satu atau lebih larangan no. 10 s.d. 17)'
    	]);

    	Tatibempat::create([
    		'isi_tatib' =>'9. Sanksi skorsing diberikan pada peserta didik yang melakukan pelanggaran berat (melanggar salah satu atau lebih larangan no. 14 atau 15)'
    	]);

    	Tatibempat::create([
    		'isi_tatib' =>'10. Sanksi dikeluarkan dari sekolah diberikan apabila peserta didik mendapatkan sanksi pemanggilan orang tua/wali lebih dari tiga kali dalam satu bulan, melakukan pelanggaran berat, ikut serta memprovokasi, menghasut, atau menggerakkan orang lain dalam perkelahian massal dengan membawa nama sekolah, serta terlibat masalah hukum (pidana) dan dinyatakan bersalah.'
    	]);
    }
}
