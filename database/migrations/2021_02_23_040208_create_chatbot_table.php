<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatbotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatbots', function (Blueprint $table) {
            $table->increments('id_chatbot');
            $table->unsignedBigInteger('sesichat_id')->nullable();;
            $table->foreign('sesichat_id')->references('id_sesi')->on('sesichats')->onDelete('set null');
            $table->boolean('is_relevant')->nullable();
            $table->unsignedBigInteger('id_pertanyaan')->nullable();
            $table->foreign('id_pertanyaan')->references('id_pertanyaan')->on('pertanyaanbots')->onDelete('set null');
            $table->string('alasan_relevan')->nullable();
             $table->date('tanggal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatbot');
    }
}
