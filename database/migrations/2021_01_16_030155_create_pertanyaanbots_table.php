<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanbotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaanbots', function (Blueprint $table) {
           //$table->increments('id_pertanyaan');
            $table->unsignedBigInteger('id_pertanyaan')->autoIncrement();
            $table->string('pertanyaan');
            $table->integer('id_topik');
            $table->longText('jawaban');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaanbots');
    }
}
