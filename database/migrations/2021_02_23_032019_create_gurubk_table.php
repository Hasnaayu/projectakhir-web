<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGurubkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gurubks', function (Blueprint $table) {
            $table->increments('id_gurubk');
            $table->string('nama_gurubk');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('no_telp');
            $table->string('nik');
            $table->string('alamat');
            $table->string('kode_pos');
            $table->integer('id_account');
            //$table->foreign('id_account')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gurubk');
    }
}
