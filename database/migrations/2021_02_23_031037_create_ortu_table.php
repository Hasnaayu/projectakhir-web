<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrtuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ortus', function (Blueprint $table) {
            $table->increments('id_ortu');
            $table->string('nama_ayah')->nullable();
            $table->string('nama_ibu');
            $table->string('nama_wali')->nullable();
            $table->integer('tahun_lahir_ayah')->nullable();
            $table->integer('tahun_lahir_ibu')->nullable();
            $table->integer('tahun_lahir_wali')->nullable();
            $table->string('jenjang_pendidikan_ayah')->nullable();
            $table->string('jenjang_pendidikan_ibu')->nullable();
            $table->string('jenjang_pendidikan_wali')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('penghasilan_ayah')->nullable();
            $table->string('penghasilan_ibu')->nullable();
            $table->string('penghasilan_wali')->nullable();
            $table->string('nik_ayah')->nullable();
            $table->string('nik_ibu')->nullable();
            $table->string('nik_wali')->nullable();
            $table->integer('id_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ortu');
    }
}
