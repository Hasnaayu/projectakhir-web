<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id_admin');
            $table->string('nama_admin');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('no_telp');
            $table->string('nik');
            $table->string('alamat');
            $table->string('kode_pos');
            $table->string('status_admin');
            $table->integer('id_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
