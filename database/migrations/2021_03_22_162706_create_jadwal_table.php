<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id_jadwal');
            $table->date('tanggal');
            $table->string('topik');
            //$table->string('hari');
            $table->boolean('isConfirmed')->nullable();
            $table->integer('id_gurubk')->nullable();
            $table->integer('id_siswa');
            $table->string('nama_siswa')->nullable();

            //$table->foreign('id_gurubk')->references('id_gurubk')->on('gurus')->onDelete('cascade');
            //$table->foreign('id_siswa')->references('id_siswa')->on('siswas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal');
    }
}
