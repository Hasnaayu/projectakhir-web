<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanggilanortuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panggilanortus', function (Blueprint $table) {
           $table->increments('id_panggilan');
            $table->date('tanggal');
            $table->string('topik');
            $table->boolean('isDone')->nullable();
            $table->integer('id_gurubk')->nullable();
            $table->integer('id_siswa');
            $table->string('nama_siswa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panggilanortu');
    }
}
