@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Orang Tua')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/ortu">Kembali</a>
</div>
<div class="box-body">
  @foreach($ortu as $o)
  <form action="/ortu/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
     <input class="form-control" type="hidden" name="id_ortu" id="id" value="{{ $o->id_ortu}}">

     <label for="name">Nama Ayah</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_ayah" id="name" value="{{ $o->nama_ayah }}" placeholder="Masukkan Nama Ayah"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="name">Nama Ibu</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_ibu" id="name" value="{{ $o->nama_ibu }}" placeholder="Masukkan Nama Ibu"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="name">Nama Wali</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_wali" id="name" value="{{ $o->nama_wali }}" placeholder="Masukkan Nama Wali"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Ayah</label>
    <input class="form-control @error('year') is-invalid @enderror" type="text" name="tahun_lahir_ayah" id="date" value="{{ $o->tahun_lahir_ayah }}" placeholder="Masukkan Tahun Lahir Ayah"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Ibu</label>
    <input class="form-control @error('year') is-invalid @enderror" type="text" name="tahun_lahir_ibu" id="date" value="{{ $o->tahun_lahir_ibu }}" placeholder="Masukkan Tahun Lahir Ibu"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Wali</label>
    <input class="form-control @error('year') is-invalid @enderror" type="text" name="tahun_lahir_wali" id="date" value="{{ $o->tahun_lahir_wali }}" placeholder="Masukkan Tahun Lahir Wali"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Ayah</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_ayah" id="pend" value="{{ $o->jenjang_pendidikan_ayah }}" placeholder="Masukkan Jenjang Pendidikan Ayah"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Ibu</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_ibu" id="pend" value="{{ $o->jenjang_pendidikan_ibu }}" placeholder="Masukkan Jenjang Pendidikan Ibu"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Wali</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_wali" id="pend" value="{{ $o->jenjang_pendidikan_wali }}" placeholder="Masukkan Jenjang Pendidikan Wali"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Ayah</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_ayah" id="work" value="{{ $o->pekerjaan_ayah }}" placeholder="Masukkan Pekerjaan Ayah"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Ibu</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_ibu" id="work" value="{{ $o->pekerjaan_ibu }}" placeholder="Masukkan Pekerjaan Ibu"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Wali</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_wali" id="work" value="{{ $o->pekerjaan_wali }}" placeholder="Masukkan Pekerjaan Wali"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Ayah</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_ayah" id="salary" value="{{ $o->penghasilan_ayah }}" placeholder="Masukkan Penghasilan Ayah"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Ibu</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_ibu" id="salary" value="{{ $o->penghasilan_ibu }}" placeholder="Masukkan Penghasilan Ibu"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Wali</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_wali" id="salary" value="{{ $o->penghasilan_wali }}" placeholder="Masukkan Penghasilan Wali"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="nik">NIK Ayah</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik_ayah" id="nik" value="{{ $o->nik_ayah }}" placeholder="Masukkan NIK Ayah"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK Ibu</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik_ibu" id="nik" value="{{ $o->nik_ibu }}" placeholder="Masukkan NIK Ibu"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK Wali</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik_wali" id="nik" value="{{ $o->nik_wali }}" placeholder="Masukkan NIK Wali"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" value="{{ $o->id_account }}" placeholder="Masukkan id account"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection