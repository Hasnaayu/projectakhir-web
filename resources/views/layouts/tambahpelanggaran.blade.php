@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Pelanggaran')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/pelanggaran">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/pelanggaran/store" method="post">
    {{ csrf_field() }}
    <!-- <div class="form-group">
      <label for="id">ID Pelanggaran</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_pelanggaran" id="id" placeholder="Masukkan ID Pelanggaran"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div> -->

    <div class="form-group">
      <label for="date">Tanggal Pelanggaran</label>
      <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_pelanggaran" id="date"> @error('date')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
     <label for="desc">Pelanggaran</label>
    <input class="form-control @error('desc') is-invalid @enderror" type="text" name="pelanggaran" id="desc" placeholder="Masukkan Pelanggaran"> @error('desc')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
     <label for="tindakan">Tindak Lanjut</label>
    <input class="form-control @error('tindakan') is-invalid @enderror" type="text" name="tindak_lanjut" id="tindakan" placeholder="Masukkan Tindak Lanjut"> @error('tindakan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="id">ID Siswa</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_siswa" id="id" placeholder="Masukkan id siswa"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="id">ID Guru BK</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_guruBK" id="id" placeholder="Masukkan id Guru BK"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection