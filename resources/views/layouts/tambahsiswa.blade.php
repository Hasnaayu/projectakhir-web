@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Siswa')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/siswa">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/siswa/store" method="post">
    {{ csrf_field() }}

    <!-- <div class="form-group">
      <label for="id">ID Siswa</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_siswa" id="id" placeholder="Masukkan ID Admin"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div> -->

    <div class="form-group">
      <label for="name">Nama Siswa</label>
      <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_siswa" id="name" placeholder="Masukkan Nama Siswa"> @error('name')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="nisn">NISN</label>
      <input class="form-control @error('nisn') is-invalid @enderror" type="text" name="nisn" id="nisn" placeholder="Masukkan NISN"> @error('nisn')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="city">Tempat Lahir</label>
      <input class="form-control @error('city') is-invalid @enderror" type="text" name="tempat_lahir" id="city" placeholder="Masukkan Tempat Lahir"> @error('city')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="date">Tanggal Lahir</label>
      <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_lahir" id="date"> @error('date')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="gender">Jenis Kelamin</label>
      <input class="form-control @error('gender') is-invalid @enderror" type="text" name="jenis_kelamin" id="gender" placeholder="Masukkan Jenis Kelamin"> @error('gender')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="nik">NIK</label>
      <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik" id="nik" placeholder="Masukkan NIK"> @error('nik')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="agama">Agama</label>
      <input class="form-control @error('agama') is-invalid @enderror" type="text" name="agama" id="agama" placeholder="Masukkan Agama"> @error('agama')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
    <label for="address">Alamat</label>
    <input class="form-control @error('address') is-invalid @enderror" type="text" name="alamat" id="address"  placeholder="Masukkan Alamat"> @error('address')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="rt">RT</label>
    <input class="form-control @error('rt') is-invalid @enderror" type="number" name="rt" id="rt" placeholder="Masukkan RT"> @error('rt')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="rw">RW</label>
    <input class="form-control @error('rw') is-invalid @enderror" type="number" name="rw" id="rw" placeholder="Masukkan RW"> @error('rw')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="dusun">Dusun</label>
    <input class="form-control @error('dusun') is-invalid @enderror" type="text" name="dusun" id="dusun" placeholder="Masukkan Dusun"> @error('dusun')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="kelurahan">Kelurahan</label>
    <input class="form-control @error('kelurahan') is-invalid @enderror" type="text" name="kelurahan" id="kelurahan" placeholder="Masukkan Kelurahan"> @error('kelurahan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="kecamatan">Kecamatan</label>
    <input class="form-control @error('kecamatan') is-invalid @enderror" type="text" name="kecamatan" id="kecamatan" placeholder="Masukkan Kecamatan"> @error('kecamatan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="pos">Kode Pos</label>
    <input class="form-control @error('pos') is-invalid @enderror" type="number" name="kode_pos" id="pos" placeholder="Masukkan Kode Pos"> @error('pos')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="telp">Nomor Telepon</label>
    <input class="form-control @error('telp') is-invalid @enderror" type="number" name="no_telp" id="telp"  placeholder="Masukkan Nomor Telepon"> @error('telp')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="tinggal">Jenis Tinggal</label>
    <input class="form-control @error('tinggal') is-invalid @enderror" type="text" name="jenis_tinggal" id="tinggal" placeholder="Masukkan Jenis Tinggal"> @error('tinggal')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="sekolah">Asal Sekolah</label>
    <input class="form-control @error('sekolah') is-invalid @enderror" type="text" name="asal_sekolah" id="sekolah" placeholder="Masukkan Asal Sekolah"> @error('sekolah')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="anakke">Anak Ke-</label>
    <input class="form-control @error('anakke') is-invalid @enderror" type="number" name="anak_keberapa" id="anakke" placeholder="Masukkan Anak Ke-"> @error('anakke')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="saudara">Jumlah Saudara Kandung</label>
    <input class="form-control @error('saudara') is-invalid @enderror" type="number" name="jumlah_saudara_kandung" id="saudara" placeholder="Masukkan Jumlah Saudara kandung"> @error('saudara')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="ortu">ID Ortu</label>
    <input class="form-control @error('ortu') is-invalid @enderror" type="number" name="id_ortu" id="ortu" placeholder="Masukkan ID Ortu"> @error('ortu')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="kelas">ID Kelas</label>
    <input class="form-control @error('kelas') is-invalid @enderror" type="number" name="id_kelas" id="kelas" placeholder="Masukkan ID Kelas"> @error('kelas')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" placeholder="Masukkan ID Account"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

<!--     <div class="form-group">
    <label for="foto">Foto</label>
    <input class="form-control @error('foto') is-invalid @enderror" type="file" name="foto" id="foto" placeholder="Masukkan foto"> @error('foto')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div> -->

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection