@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Data Siswa')

@section('content')
<!-- Default box -->
<div class="box-header" style="margin-bottom: 16px">
  <a class="btn btn-success btn-flat" href="/siswa/tambahsiswa"> + Tambah Data Siswa Baru</a>
</div>
    <div class="button" style="margin-bottom: 8px; margin-left: 10px">
      <form action="/users/import" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group"> 
        <input type="file" name="import_file" style="margin-bottom: 10px">
        <button type="submit" class="btn btn-success btn-flat" style="margin-right: 36px"> Import File Data</button>
        <a class="btn btn-success btn-flat" href="/siswaexports"> Eksport File Data</a>
      </div>
      </form>
    </div>

    <div class="box-body">
      <table border="1">
        <tr>
         <!--  <th>ID Siswa</th> -->
          <th>Nama Siswa</th>
          <th>NISN</th>
          <th>Tempat Lahir</th>
          <th>Tanggal Lahir</th>
          <th>Jenis Kelamin</th>
          <th>NIK</th>
          <th>Agama</th>
          <th>Alamat</th>
          <th>RT</th>
          <th>RW</th>
          <th>Dusun</th>
          <th>Kelurahan</th>
          <th>Kecamatan</th>
          <th>Kode Pos</th>
          <th>Nomor Telepon</th>
          <th>Jenis Tinggal</th>
          <th>Asal Sekolah</th>
          <th>Anak Ke-</th>
          <th>Jumlah Saudara Kandung</th>
          <th>ID Orang Tua</th>
          <th>ID Kelas</th>
          <th>ID Account</th>
          <th>Opsi</th>
        </tr>
        @foreach($siswa as $s)
        <tr>
          <!-- <td>{{ $s->id_siswa }}</td> -->
          <td>{{ $s->nama_siswa }}</td>
          <td>{{ $s->nisn }}</td>
          <td>{{ $s->tempat_lahir }}</td>
          <td>{{ $s->tanggal_lahir }}</td>
          <td>{{ $s->jenis_kelamin }}</td>
          <td>{{ $s->nik }}</td>
          <td>{{ $s->agama }}</td>
          <td>{{ $s->alamat }}</td>
          <td>{{ $s->rt }}</td>
          <td>{{ $s->rw }}</td>
          <td>{{ $s->dusun }}</td>
          <td>{{ $s->kelurahan }}</td>
          <td>{{ $s->kecamatan }}</td>
          <td>{{ $s->kode_pos }}</td>
          <td>{{ $s->no_telp }}</td>
          <td>{{ $s->jenis_tinggal }}</td>
          <td>{{ $s->asal_sekolah }}</td>
          <td>{{ $s->anak_keberapa }}</td>
          <td>{{ $s->jumlah_saudara_kandung }}</td>
          <td>{{ $s->id_ortu }}</td>
          <td>{{ $s->id_kelas }}</td>
          <td>{{ $s->id_account }}</td>
          <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/siswa/editsiswa/{{ $s->id_siswa }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/siswa/hapussiswa/{{ $s->id_siswa }}">Hapus</a>
            </span>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection