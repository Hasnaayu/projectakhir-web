@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Pelanggaran')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <!-- <a class="btn btn-success btn-flat" href="/pelanggaran/tambahpelanggaran"> + Tambah Data Pelanggaran</a> -->
    </div>
    <!-- <div class="button" style="margin-bottom: 8px; margin-left: 10px">
      <form action="/pelanggaranimports" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group"> 
        <input type="file" name="import_file" style="margin-bottom: 10px">
        <button type="submit" class="btn btn-success btn-flat" style="margin-right: 36px"> Import File Data</button>
        <a class="btn btn-success btn-flat" href="/pelanggaranexports"> Eksport File Data</a>
      </div>
      </form>
    </div> -->
    <div class="box-body">
      <table border="1">
        <tr>
          <!-- <th>ID Pelanggaran</th> -->
          <th>Tanggal Pelanggaran</th>
          <th>Pelanggaran</th>
          <th>Tindak Lanjut</th>
          <th>ID Siswa</th>
          <th>ID Guru BK</th>
          <!-- <th>Opsi</th> -->
        </tr>
        @foreach($pelanggaran as $p)
        <tr>
          <!-- <td>{{ $p->id_pelanggaran }}</td> -->
          <td>{{ $p->tanggal_pelanggaran }}</td>
          <td>{{ $p->pelanggaran }}</td>
          <td>{{ $p->tindak_lanjut }}</td>
          <td>{{ $p->id_siswa }}</td>
          <td>{{ $p->id_guruBK }}</td>
          <!-- <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/pelanggaran/editpelanggaran/{{ $p->id_pelanggaran }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/pelanggaran/hapuspelanggaran/{{ $p->id_pelanggaran }}">Hapus</a>
            </span>
          </td> -->
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection