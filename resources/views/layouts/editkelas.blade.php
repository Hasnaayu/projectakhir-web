@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Kelas')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/kelas">Kembali</a>
</div>
<div class="box-body">
  @foreach($kelas as $k)
  <form action="/kelas/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
    <input class="form-control" type="hidden" name="id_kelas" id="id" value="{{ $k->id_kelas}}">

    <label for="name">Kelas</label>
    <input class="form-control @error('name') is-invalid @enderror" type="text" name="kelas" id="name" value="{{ $k->kelas }}" placeholder="Masukkan Kelas Siswa"> @error('name')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="jurusan">Jurusan</label>
    <input class="form-control @error('jurusan') is-invalid @enderror" type="text" name="jurusan" id="jurusan" value="{{ $k->jurusan }}"> @error('jurusan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="id">ID Guru BK</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_guruBK" id="id" value="{{ $k->id_guruBK }}" placeholder="Masukkan id guru BK"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-primary">
        Simpan Data
      </button>
    </div>
  </div>
</form>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection