@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Data Siswa')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/siswa">Kembali</a>
</div>
<div class="box-body">
  @foreach($siswa as $s)
  <form action="/siswa/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
     <input class="form-control" type="hidden" name="id_siswa" id="id" value="{{ $s->id_siswa}}">

     <label for="name">Nama Siswa</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_siswa" id="name" value="{{ $s->nama_siswa }}" placeholder="Masukkan Nama Siswa"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="nisn">NISN</label>
    <input class="form-control @error('nisn') is-invalid @enderror" type="text" name="nisn" id="nisn" placeholder="Masukkan NISN" value="{{ $s->nisn }}"> @error('nisn')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="city">Tempat Lahir</label>
    <input class="form-control @error('city') is-invalid @enderror" type="text" name="tempat_lahir" id="city" placeholder="Masukkan Tempat Lahir" value="{{ $s->tempat_lahir }}"> @error('city')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="date">Tanggal Lahir</label>
    <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_lahir" id="date" placeholder="Masukkan Tanggal Lahir" value="{{ $s->tanggal_lahir }}"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="gender">Jenis Kelamin</label>
    <input class="form-control @error('gender') is-invalid @enderror" type="text" name="jenis_kelamin" id="gender" placeholder="Masukkan Jenis Kelamin" value="{{ $s->jenis_kelamin }}"> @error('gender')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik" id="nik" placeholder="Masukkan NIK" value="{{ $s->nik }}"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="agama">Agama</label>
    <input class="form-control @error('agama') is-invalid @enderror" type="text" name="agama" id="agama" placeholder="Masukkan Agama" value="{{ $s->agama }}"> @error('agama')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="address">Alamat</label>
    <input class="form-control @error('address') is-invalid @enderror" type="text" name="alamat" id="address"  placeholder="Masukkan Alamat" value="{{ $s->alamat }}"> @error('address')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="rt">RT</label>
    <input class="form-control @error('rt') is-invalid @enderror" type="number" name="rt" id="rt" placeholder="Masukkan RT" value="{{ $s->rt }}"> @error('rt')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="rw">RW</label>
    <input class="form-control @error('rw') is-invalid @enderror" type="number" name="rw" id="rw" placeholder="Masukkan RW" value="{{ $s->rw }}"> @error('rw')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="dusun">Dusun</label>
    <input class="form-control @error('dusun') is-invalid @enderror" type="text" name="dusun" id="dusun" placeholder="Masukkan Dusun" value="{{ $s->dusun }}"> @error('dusun')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="kelurahan">Kelurahan</label>
    <input class="form-control @error('kelurahan') is-invalid @enderror" type="text" name="kelurahan" id="kelurahan" placeholder="Masukkan Kelurahan" value="{{ $s->kelurahan }}"> @error('kelurahan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="kecamatan">Kecamatan</label>
    <input class="form-control @error('kecamatan') is-invalid @enderror" type="text" name="kecamatan" id="kecamatan" placeholder="Masukkan Kecamatan" value="{{ $s->kecamatan }}"> @error('kecamatan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="pos">Kode Pos</label>
    <input class="form-control @error('pos') is-invalid @enderror" type="number" name="kode_pos" id="pos" placeholder="Masukkan Kode Pos" value="{{ $s->kode_pos }}"> @error('pos')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="telp">Nomor Telepon</label>
    <input class="form-control @error('telp') is-invalid @enderror" type="number" name="no_telp" id="telp"  placeholder="Masukkan Nomor Telepon" value="{{ $s->no_telp }}"> @error('telp')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tinggal">Kecamatan</label>
    <input class="form-control @error('tinggal') is-invalid @enderror" type="text" name="jenis_tinggal" id="tinggal" placeholder="Masukkan Jenis Tinggal" value="{{ $s->jenis_tinggal }}"> @error('tinggal')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="sekolah">Asal Sekolah</label>
    <input class="form-control @error('sekolah') is-invalid @enderror" type="text" name="asal_sekolah" id="sekolah" placeholder="Masukkan Asal Sekolah" value="{{ $s->asal_sekolah }}"> @error('sekolah')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="anakke">Anak Ke-</label>
    <input class="form-control @error('anakke') is-invalid @enderror" type="number" name="anak_keberapa" id="anakke" placeholder="Masukkan Anak Ke-" value="{{ $s->anak_keberapa }}"> @error('anakke')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="saudara">Jumlah Saudara Kandung</label>
    <input class="form-control @error('saudara') is-invalid @enderror" type="number" name="jumlah_saudara_kandung" id="saudara" placeholder="Masukkan Jumlah Saudara kandung" value="{{ $s->jumlah_saudara_kandung }}"> @error('saudara')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="ortu">ID Ortu</label>
    <input class="form-control @error('ortu') is-invalid @enderror" type="number" name="id_ortu" id="ortu" placeholder="Masukkan ID Ortu" value="{{ $s->id_ortu }}"> @error('ortu')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="kelas">ID Kelas</label>
    <input class="form-control @error('kelas') is-invalid @enderror" type="number" name="id_kelas" id="kelas" placeholder="Masukkan ID Kelas" value="{{ $s->id_kelas }}"> @error('kelas')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" placeholder="Masukkan ID Account" value="{{ $s->id_account }}"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

 <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection