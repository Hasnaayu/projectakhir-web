@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Admin')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/admin">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/admin/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="id">ID Admin</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_admin" id="id" placeholder="Masukkan ID Admin"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
     <label for="name">Nama Admin</label>
    <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_admin" id="name" placeholder="Masukkan Nama Admin"> @error('name')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="date">Tanggal Lahir</label>
    <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_lahir" id="date"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="gender">Jenis Kelamin</label>
    <input class="form-control @error('gender') is-invalid @enderror" type="text" name="jenis_kelamin" id="gender" placeholder="Masukkan jenis kelamin"> @error('gender')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="telp">Nomor Telepon</label>
    <input class="form-control @error('telp') is-invalid @enderror" type="number" name="no_telp" id="telp" placeholder="Masukkan nomor telepon"> @error('telp')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik" id="nik" placeholder="Masukkan NIK"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="address">Alamat</label>
    <input class="form-control @error('address') is-invalid @enderror" type="text" name="alamat" id="address" placeholder="Masukkan alamat"> @error('address')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="pos">Kode Pos</label>
    <input class="form-control @error('pos') is-invalid @enderror" type="number" name="kode_pos" id="pos" placeholder="Masukkan kode pos"> @error('pos')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="status">Status Admin</label>
    <input class="form-control @error('status') is-invalid @enderror" type="text" name="status_admin" id="status" placeholder="Masukkan status"> @error('status')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" placeholder="Masukkan id account"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection