@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Orang Tua')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <a class="btn btn-success btn-flat" href="/ortu/tambahortu"> + Tambah Data Orang Tua</a>
    </div>
     <div class="button" style="margin-bottom: 8px; margin-left: 10px">
      <form action="/ortuimports" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group"> 
        <input type="file" name="import_file" style="margin-bottom: 10px">
        <button type="submit" class="btn btn-success btn-flat" style="margin-right: 36px"> Import File Data</button>
        <a class="btn btn-success btn-flat" href="/ortuexports"> Eksport File Data</a>
      </div>
      </form>
    </div>

    <div class="box-body">
      <table border="1">
        <tr>
          <!-- <th>ID Orang Tua</th> -->
          <th>Nama Ayah</th>
          <th>Nama Ibu</th>
          <th>Nama Wali</th>
          <th>Tahun Lahir Ayah</th>
          <th>Tahun Lahir Ibu</th>
          <th>Tahun Lahir Wali</th>
          <th>Jenjang Pendidikan Ayah</th>
          <th>Jenjang Pendidikan Ibu</th>
          <th>Jenjang Pendidikan Wali</th>
          <th>Pekerjaan Ayah</th>
          <th>Pekerjaan Ibu</th>
          <th>Pekerjaan Wali</th>
          <th>Penghasilan Ayah</th>
          <th>Penghasilan Ibu</th>
          <th>Penghasilan Wali</th>
          <th>NIK Ayah</th>
          <th>NIK Ibu</th>
          <th>NIK Wali</th>
          <th>ID Account</th>
          <th>Opsi</th>
        </tr>
        @foreach($ortu as $o)
        <tr>
         <!--  <td>{{ $o->id_ortu }}</td> -->
          <td>{{ $o->nama_ayah }}</td>
          <td>{{ $o->nama_ibu }}</td>
          <td>{{ $o->nama_wali }}</td>
          <td>{{ $o->tahun_lahir_ayah }}</td>
          <td>{{ $o->tahun_lahir_ibu }}</td>
          <td>{{ $o->tahun_lahir_wali }}</td>
          <td>{{ $o->jenjang_pendidikan_ayah }}</td>
          <td>{{ $o->jenjang_pendidikan_ibu }}</td>
          <td>{{ $o->jenjang_pendidikan_wali }}</td>
          <td>{{ $o->pekerjaan_ayah }}</td>
          <td>{{ $o->pekerjaan_ibu }}</td>
          <td>{{ $o->pekerjaan_wali }}</td>
          <td>{{ $o->penghasilan_ayah }}</td>
          <td>{{ $o->penghasilan_ibu }}</td>
          <td>{{ $o->penghasilan_wali }}</td>
          <td>{{ $o->nik_ayah }}</td>
          <td>{{ $o->nik_ibu }}</td>
          <td>{{ $o->nik_wali }}</td>
          <td>{{ $o->id_account }}</td>
          <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/ortu/editortu/{{ $o->id_ortu }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/ortu/hapusortu/{{ $o->id_ortu }}">Hapus</a>
            </span>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection