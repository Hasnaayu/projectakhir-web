<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> @yield('title') </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--Logo Title Bar-->
    <link rel="shortcut icon" href="{{ asset('dist/img/logo.ico') }}" type="image/x-icon">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('fonts/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
    <!-- Form -->
    <link rel="stylesheet" href="{{ asset('dist/css/form.css') }}">
    <!-- Grafik -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="hold-transition skin-purple sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <span class="logo-mini"><b>A</b></span>
                <span class="logo-lg">Admin</span>
            </a>
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('dist/img/user2.png') }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">
                                 @if(auth()->check())
                                 <b>{{ auth()->user()->name }}</b>
                                 @else
                                 Guest
                                 @endif
                             </span>
                         </a>
                         <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Sign out') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
</header>


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/user2.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @if(auth()->user())
                <p><b> {{ auth()->user()->name }} </b></p>
                @else
                <p>Guest</p>
                @endif
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="bg-black">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fa fa-address-book fa-fw mr-3"></span>
                        <span class="menu-collapsed"><b>Manajemen Data</b></span>
                        <span class="submenu-icon ml-auto"></span>
                    </div>
                </a>
                <div id='submenu1' class="collapse sidebar-submenu">
                    <a href="/siswa" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"> <b>Data Siswa</b> </span>
                    </a>
                    <a href="/gurubk" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Data Guru BK</b></span>
                    </a>
                    <a href="/ortu" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Data Orang Tua</b></span>
                    </a>
                    <a href="/pelanggaran" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Data Pelanggaran</b></span>
                    </a>
                    <a href="/bimbingan" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Data Bimbingan</b></span>
                    </a>
                </div>
                <a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="bg-black">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fa fa-users fa-fw mr-3"></span>
                        <span class="menu-collapsed"> <b>Manajemen User</b> </span>
                        <span class="submenu-icon ml-auto"></span>
                    </div>
                </a>
                <div id='submenu2' class="collapse sidebar-submenu">
                    <a href="/daftar/akun/siswa" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Siswa</b></span>
                    </a>
                    <a href="/daftar/akun/gurubk" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Guru BK</b></span>
                    </a>
                    <a href="/daftar/akun/admin" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Admin</b></span>
                    </a>
                    <a href="/daftar/akun/ortu" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Orang Tua</b></span>
                    </a>
                </div>
                <a href="#submenu3" data-toggle="collapse" aria-expanded="false" class="bg-black">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fa fa-line-chart fa-fw mr-3"></span>
                        <span class="menu-collapsed"> <b>Statistik</b> </span>
                        <span class="submenu-icon ml-auto"></span>
                    </div>
                </a>
                <div id='submenu3' class="collapse sidebar-submenu">
                    <a href="/pelanggaran/grafikpelanggarankelas" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Tiap Kelas</b></span>
                    </a>
                    <a href="/pelanggaran/grafikpelanggaransiswa" class="list-group-item list-group-item-action bg-dark text-black">
                        <span class="menu-collapsed"><b>Tiap Siswa</b></span>
                    </a>
                </div>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('page-title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b><strong> <a href="#">SMK PSM 1 Kawedanan</a></strong></b>
        </div>
        <strong> <a href="#">@smkpsm1kwd</a></strong>
    </footer>
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
</body>
</html>
