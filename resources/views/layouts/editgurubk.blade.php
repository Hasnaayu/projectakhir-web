@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Guru BK')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/gurubk">Kembali</a>
</div>
<div class="box-body">
  @foreach($gurubk as $g)
  <form action="/gurubk/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
     <input class="form-control" type="hidden" name="id_gurubk" id="id" value="{{ $g->id_gurubk}}">

     <label for="name">Nama gurubk</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_gurubk" id="name" value="{{ $g->nama_gurubk }}" placeholder="Masukkan Nama Guru BK"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="date">Tanggal Lahir</label>
    <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_lahir" id="date" value="{{ $g->tanggal_lahir }}"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="gender">Jenis Kelamin</label>
    <input class="form-control @error('gender') is-invalid @enderror" type="text" name="jenis_kelamin" id="gender" value="{{ $g->jenis_kelamin }}" placeholder="Masukkan jenis kelamin"> @error('gender')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="telp">Nomor Telepon</label>
    <input class="form-control @error('telp') is-invalid @enderror" type="number" name="no_telp" id="telp" value="{{ $g->no_telp }}" placeholder="Masukkan nomor telepon"> @error('telp')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik" id="nik" value="{{ $g->nik }}" placeholder="Masukkan NIK"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="address">Alamat</label>
    <input class="form-control @error('address') is-invalid @enderror" type="text" name="alamat" id="address" value="{{ $g->alamat }}" placeholder="Masukkan alamat"> @error('address')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="pos">Kode Pos</label>
    <input class="form-control @error('pos') is-invalid @enderror" type="number" name="kode_pos" id="pos" value="{{ $g->kode_pos }}" placeholder="Masukkan kode pos"> @error('pos')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" value="{{ $g->id_account }}" placeholder="Masukkan id account"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection