@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Guru BK')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/gurubk">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/gurubk/store" method="post">
    {{ csrf_field() }}

    <!-- <div class="form-group">
      <label for="id">ID Guru BK</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_gurubk" id="id" placeholder="Masukkan ID Guru BK"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div> -->


    <div class="form-group">
      <label for="name">Nama Guru BK</label>
      <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_gurubk" id="name" placeholder="Masukkan Nama Guru BK"> @error('name')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="date">Tanggal Lahir</label>
      <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_lahir" id="date"> @error('date')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="gender">Jenis Kelamin</label>
      <input class="form-control @error('gender') is-invalid @enderror" type="text" name="jenis_kelamin" id="gender" placeholder="Masukkan Jenis Kelamin"> @error('gender')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="telp">Nomor Telepon</label>
      <input class="form-control @error('telp') is-invalid @enderror" type="number" name="no_telp" id="telp" placeholder="Masukkan nomor telepon"> @error('telp')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="nik">NIK</label>
      <input class="form-control @error('nik') is-invalid @enderror" type="number" name="nik" id="nik" placeholder="Masukkan NIK"> @error('nik')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="address">Alamat</label>
      <input class="form-control @error('address') is-invalid @enderror" type="text" name="alamat" id="address"  placeholder="Masukkan Alamat"> @error('address')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="pos">Kode Pos</label>
      <input class="form-control @error('pos') is-invalid @enderror" type="number" name="kode_pos" id="pos" placeholder="Masukkan Kode Pos"> @error('pos')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="account">ID Account</label>
      <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" placeholder="Masukkan ID Account"> @error('account')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection