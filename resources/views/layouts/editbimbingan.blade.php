@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Bimbingan')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/bimbingan">Kembali</a>
</div>
<div class="box-body">
  @foreach($bimbingan as $b)
  <form action="/bimbingan/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
     <input class="form-control" type="hidden" name="id_bimbingan" id="id" value="{{ $b->id_bimbingan}}">

     <label for="date">Tanggal Bimbingan</label>
     <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_bimbingan" id="date" value="{{ $b->tanggal_bimbingan}}"> @error('date')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
     <label for="desc">Permasalahan</label>
     <input class="form-control @error('desc') is-invalid @enderror" type="text" name="permasalahan" id="desc" value="{{ $b->permasalahan}}"placeholder="Masukkan Permasalahan"> @error('desc')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
     <label for="tindakan">Solusi</label>
     <input class="form-control @error('tindakan') is-invalid @enderror" type="text" name="solusi" id="tindakan" value="{{ $b->solusi}}" placeholder="Masukkan Solusi"> @error('tindakan')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="account">ID Siswa</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_siswa" id="account" value="{{ $b->id_siswa}}" placeholder="Masukkan id siswa"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Guru BK</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_guruBK" id="account" value="{{ $b->id_guruBK}}" placeholder="Masukkan id Guru BK"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection