@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Kelas')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <a class="btn btn-success btn-flat" href="/kelas/tambahkelas"> + Tambah Data Kelas</a>
    </div>
    <div class="box-body">
      <table border="1">
        <tr>
          <th>ID Kelas</th>
          <th>Kelas</th>
          <th>Jurusan</th>
          <th>ID Guru BK</th>
          <th>Opsi</th>
        </tr>
        @foreach($kelas as $k)
        <tr>
          <td>{{ $k->id_kelas }}</td>
          <td>{{ $k->kelas }}</td>
          <td>{{ $k->jurusan }}</td>
          <td>{{ $k->id_guruBK }}</td>
          <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/kelas/editkelas/{{ $k->id_kelas }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/kelas/hapuskelas/{{ $k->id_kelas }}">Hapus</a>
            </span>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection