@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Kelas')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/kelas">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/kelas/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="id">ID Kelas</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_kelas" id="id" placeholder="Masukkan ID Kelas"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

     <div class="form-group">
    <label for="kelas">Kelas</label>
    <input class="form-control @error('kelas') is-invalid @enderror" type="number" name="kelas" id="kelas" placeholder="Masukkan kelas siswa"> @error('kelas')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
     <label for="jurusan">Jurusan</label>
    <input class="form-control @error('jurusan') is-invalid @enderror" type="text" name="jurusan" id="jurusan" placeholder="Masukkan Jurusan"> @error('jurusan')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="form-group">
    <label for="id">ID Guru BK</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_gurubk" id="id" placeholder="Masukkan ID Guru BK"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection