@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Grafik Pelanggaran Per Siswa')

@section('content')
<div class="row">
  <div class="col-md-offset-1">
    <label for="filter">Kelas:</label>
    <select class="filter-kelas">
      @foreach ($data_kelas as $dkelas)
      <option value="{{$dkelas->id_kelas}}">{{ $dkelas->kelas }} {{ $dkelas->jurusan}}
      </option>
      @endforeach
    </select>
  </div>
  <div class="col-md-offset-1">
    <label for="filter">Siswa:</label>
    <select class="filter-siswa" id="filterSiswa">
    </select>
  </div>
  <div class="col-md-offset-1">
    <label for="filter">Tahun:</label>
    <select class="filter-year" id="filterYear">
    </select>
  </div>
<div class="row">

 <div class="col-md-10 col-md-offset-1">
   <div class="panel panel-default">
     <div class="panel-heading"><b>Charts</b></div>
     <div class="panel-body">
       <canvas id="canvas" height="280" width="600"></canvas>
     </div>
   </div>
 </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<script>

  var Months = new Array();
  var Classes = new Array();
  var Values = new Array();
  $(document).ready(function(){
    var idKelas = 1;
    var idSiswa = 1;
    var tahun = 2020;
    var baseURL = 'http://localhost:8000'
    function getDataSiswa(idKelas){
      var urlSiswa = baseURL + '/api/siswa/kelas/'+idKelas;
      $.get(urlSiswa, function(response){
        $('#filterSiswa option').remove();
        response.data.forEach(function(data){
          $('#filterSiswa').
          append(`<option value="${data.id_siswa}">
           ${data.nama_siswa}
           </option>`);
        })
      });
    }
    
    getDataSiswa(idKelas);
    //displayGrafik(idSiswa, tahun);

    function getDataYear() {
      const today = new Date();
      const startYear = 2020;
      for(let i = today.getFullYear(); i >= startYear; i--){
        $('#filterYear').append(yearToAppend(i));
      }
    }
    function yearToAppend(tanggal_pelanggaran) {
      if (tanggal_pelanggaran === tahun) return `<option value="${tanggal_pelanggaran}" selected>${tanggal_pelanggaran}</option>`;
      return `<option value="${tanggal_pelanggaran}">${tanggal_pelanggaran}</option>`;
    }

    getDataYear();
    displayGrafik(idSiswa, tahun);

    $(".filter-siswa").change(function() {
      Months = [];
      Values = [];
      var idSiswaFiltered = $(this).val();
      idSiswa = idSiswaFiltered;
      //console.log('changed!')
      displayGrafik(idSiswaFiltered, tahun);
    });
    $(".filter-kelas").change(function() {
      Months = [];
      Values = [];
      var idKelasFiltered = $(this).val();
      getDataSiswa(idKelasFiltered);
      var UrlSiswaBaru = baseURL + '/api/siswa/kelas/'+idKelasFiltered;
       
      $.get(UrlSiswaBaru, function(response){
        idSiswa = response.data[0].id_siswa;
        displayGrafik(response.data[0].id_siswa, tahun);
      });
    });

    $(".filter-year").change(function() {
      Months = [];
      Values = [];
      var yearFiltered = $(this).val();
      tahun = yearFiltered;
      console.log(idSiswa)
      displayGrafik(idSiswa, yearFiltered);
    });

    function displayGrafik(id_siswa, id_tahun){
      var url = baseURL+`/api/pelanggaran/grafik/siswa/${id_siswa}/${id_tahun}`;  
      $.get(url, function(response){
        response.data.forEach(function(data){
          Months.push(data.bulan);
          Values.push(data.pelanggaran);
        });
        var ctx = document.getElementById("canvas").getContext('2d');
        if (window.MyChart != undefined) {
          window.MyChart.destroy();
        }
        window.MyChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels:Months,
            datasets: [{
              label: 'Pelanggaran',
              data: Values,
              borderWidth: 1,
              backgroundColor : "grey"
            }],
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            }
          }
      // });
    });
      });
    }

  });
</script>
</body>
</html>
@endsection