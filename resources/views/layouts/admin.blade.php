@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Admin')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <a class="btn btn-success btn-flat" href="/admin/tambahadmin"> + Tambah Data Admin</a>
    </div>
    <div class="box-body">
      <table border="1">
        <tr>
          <th>ID Admin</th>
          <th>Nama Admin</th>
          <th>Tanggal Lahir</th>
          <th>Jenis Kelamin</th>
          <th>Nomor Telepon</th>
          <th>NIK</th>
          <th>Alamat</th>
          <th>Kode Pos</th>
          <th>Status</th>
          <th>ID Account</th>
          <th>Opsi</th>
        </tr>
        @foreach($admin as $a)
        <tr>
          <td>{{ $a->id_admin }}</td>
          <td>{{ $a->nama_admin }}</td>
          <td>{{ $a->tanggal_lahir }}</td>
          <td>{{ $a->jenis_kelamin }}</td>
          <td>{{ $a->no_telp }}</td>
          <td>{{ $a->nik }}</td>
          <td>{{ $a->alamat }}</td>
          <td>{{ $a->kode_pos }}</td>
          <td>{{ $a->status_admin }}</td>
          <td>{{ $a->id_account }}</td>
          <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/admin/editadmin/{{ $a->id_admin }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/admin/hapusadmin/{{ $a->id_admin }}">Hapus</a>
            </span>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection