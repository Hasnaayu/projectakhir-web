@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Bimbingan')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <!-- <a class="btn btn-success btn-flat" href="/bimbingan/tambahbimbingan"> + Tambah Data Bimbingan</a> -->
    </div>
    <div class="box-body">
      <table border="1">
        <tr>
          <!-- <th>ID Bimbingan</th> -->
          <th>Tanggal Bimbingan</th>
          <th>Permasalahan</th>
          <th>Solusi</th>
          <th>ID Siswa</th>
          <th>ID Guru BK</th>
          <!-- <th>Opsi</th> -->
        </tr>
        @foreach($bimbingan as $b)
        <tr>
          <!-- <td>{{ $b->id_bimbingan }}</td> -->
          <td>{{ $b->tanggal_bimbingan }}</td>
          <td>{{ $b->permasalahan }}</td>
          <td>{{ $b->solusi }}</td>
          <td>{{ $b->id_siswa }}</td>
          <td>{{ $b->id_guruBK }}</td>
          <!-- <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/bimbingan/editbimbingan/{{ $b->id_bimbingan }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/bimbingan/hapusbimbingan/{{ $b->id_bimbingan }}">Hapus</a>
            </span>
          </td> -->
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection