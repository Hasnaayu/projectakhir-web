@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Grafik Pelanggaran Per Kelas')

@section('content')
<div class="row">
  <div class="col-md-offset-1">
    <label for="filter">Kelas:</label>
    <select class="filter">
      @foreach ($data_kelas as $dkelas)
      <option value="{{$dkelas->id_kelas}}">{{ $dkelas->kelas }} {{ $dkelas->jurusan}}
      </option>
      @endforeach
    </select>
  </div>
  <div class="col-md-offset-1">
    <label for="filter">Tahun:</label>
    <select class="filter-year" id="filterYear">
    </select>
  </div>
  <div class="row">

   <div class="col-md-10 col-md-offset-1">
     <div class="panel panel-default">
       <div class="panel-heading"><b>Charts</b></div>
       <div class="panel-body">
         <canvas id="canvas" height="280" width="600"></canvas>
       </div>
     </div>
   </div>
 </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
 <script>

  var Months = new Array();
  var Classes = new Array();
  var Values = new Array();
  $(document).ready(function(){
    var idKelas = 1;
    var tahun = 2020;
    var baseURL = 'http://localhost:8000'

    //displayGrafik(idKelas);
    $(".filter").change(function() {
      Months = [];
      Values = [];
      var idKelasFiltered = $(this).val();
      idKelas = idKelasFiltered; 
      displayGrafik(idKelasFiltered, tahun);
    });

    $(".filter-year").change(function() {
      Months = [];
      Values = [];
      var yearFiltered = $(this).val();
      tahun = yearFiltered;
      displayGrafik(idKelasFiltered, yearFiltered);
    });

    function getDataYear() {
      const today = new Date();
      const startYear = 2020;
      for(let i = today.getFullYear(); i >= startYear; i--){
        $('#filterYear').append(yearToAppend(i));
      }
    }
    function yearToAppend(tanggal_pelanggaran) {
      if (tanggal_pelanggaran === tahun) return `<option value="${tanggal_pelanggaran}" selected>${tanggal_pelanggaran}</option>`;
      return `<option value="${tanggal_pelanggaran}">${tanggal_pelanggaran}</option>`;
    }

    getDataYear();
    displayGrafik(idKelas, tahun);

    function displayGrafik(id_kelas, tahun){
      var url = baseURL+`/api/pelanggaran/grafik/${id_kelas}/${tahun}`;  
      $.get(url, function(response){
        response.data.forEach(function(data){
          Months.push(data.bulan);
          Values.push(data.pelanggaran);
        });
        var ctx = document.getElementById("canvas").getContext('2d');
        if (window.MyChart != undefined) {
          window.MyChart.destroy();
        }
        window.MyChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels:Months,
            datasets: [{
              label: 'Pelanggaran',
              data: Values,
              borderWidth: 1,
              backgroundColor : "grey"
            }],
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            }
          }
      // });
    });
      });
    }

  });
</script>
</body>
</html>
@endsection