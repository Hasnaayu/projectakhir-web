@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Data Guru BK')

  @section('content')
  <!-- Default box -->
    <div class="box-header">
      <a class="btn btn-success btn-flat" href="/gurubk/tambahgurubk"> + Tambah Data Guru BK</a>
    </div>
    <div class="button" style="margin-bottom: 8px; margin-left: 10px">
      <form action="/gurubkimports" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group"> 
        <input type="file" name="import_file" style="margin-bottom: 10px">
        <button type="submit" class="btn btn-success btn-flat" style="margin-right: 36px"> Import File Data</button>
        <a class="btn btn-success btn-flat" href="/gurubkexports"> Eksport File Data</a>
      </div>
      </form>
    </div>
    
    <div class="box-body">
      <table border="1">
        <tr>
          <!-- <th>ID Guru BK</th> -->
          <th>Nama Guru BK</th>
          <th>Tanggal Lahir</th>
          <th>Jenis Kelamin</th>
          <th>Nomor Telepon</th>
          <th>NIK</th>
          <th>Alamat</th>
          <th>Kode Pos</th>
          <th>ID Account</th>
          <th>Opsi</th>
        </tr>
        @foreach($gurubk as $g)
        <tr>
          <!-- <td>{{ $g->id_gurubk }}</td> -->
          <td>{{ $g->nama_gurubk }}</td>
          <td>{{ $g->tanggal_lahir }}</td>
          <td>{{ $g->jenis_kelamin }}</td>
          <td>{{ $g->no_telp }}</td>
          <td>{{ $g->nik }}</td>
          <td>{{ $g->alamat }}</td>
          <td>{{ $g->kode_pos }}</td>
          <td>{{ $g->id_account }}</td>
          <td>
            <span class="input-group-btn">
              <a class="btn btn-warning btn-flat" href="/gurubk/editgurubk/{{ $g->id_gurubk }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/gurubk/hapusgurubk/{{ $g->id_gurubk }}">Hapus</a>
            </span>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection