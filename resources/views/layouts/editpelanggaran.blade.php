@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Edit Pelanggaran')

@section('content')
<!-- Default box -->
<div class="box">
 <div class="box-header">
  <a class="btn btn-success btn-flat" href="/pelanggaran">Kembali</a>
</div>
<div class="box-body">
  @foreach($pelanggaran as $p)
  <form action="/pelanggaran/update" method="post">
   {{ csrf_field() }}
   <div class="form-group">
     <input class="form-control" type="hidden" name="id_pelanggaran" id="id" value="{{ $p->id_pelanggaran}}">

     <label for="date">Tanggal Pelanggaran</label>
     <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_pelanggaran" id="date" value="{{ $p->tanggal_pelanggaran}}"> @error('date')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
     <label for="desc">Pelanggaran</label>
     <input class="form-control @error('desc') is-invalid @enderror" type="text" name="pelanggaran" id="desc" value="{{ $p->pelanggaran}}"placeholder="Masukkan Pelanggaran"> @error('desc')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
     <label for="tindakan">Tindak Lanjut</label>
     <input class="form-control @error('tindakan') is-invalid @enderror" type="text" name="tindak_lanjut" id="tindakan" value="{{ $p->tindak_lanjut}}" placeholder="Masukkan Tindak Lanjut"> @error('tindakan')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="account">ID Siswa</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_siswa" id="account" value="{{ $p->id_siswa}}" placeholder="Masukkan id siswa"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Guru BK</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_guruBK" id="account" value="{{ $p->id_guruBK}}" placeholder="Masukkan id Guru BK"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
@endforeach
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection