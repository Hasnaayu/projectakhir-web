@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Orang Tua')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/ortu">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/ortu/store" method="post">
    {{ csrf_field() }}
   <!--  <div class="form-group">
      <label for="id">ID Orang Tua</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_ortu" id="id" placeholder="Masukkan ID Orang Tua"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div> -->

    <div class="form-group">
    <label for="name">Nama Ayah</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_ayah" id="name"  placeholder="Masukkan Nama Ayah"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="name">Nama Ibu</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_ibu" id="name"  placeholder="Masukkan Nama Ibu"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="name">Nama Wali</label>
     <input class="form-control @error('name') is-invalid @enderror" type="text" name="nama_wali" id="name"  placeholder="Masukkan Nama Wali"> @error('name')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Ayah</label>
    <input class="form-control @error('year') is-invalid @enderror" type="year" name="tahun_lahir_ayah" id="date"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Ibu</label>
    <input class="form-control @error('year') is-invalid @enderror" type="year" name="tahun_lahir_ibu" id="date"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="year">Tahun Lahir Wali</label>
    <input class="form-control @error('year') is-invalid @enderror" type="year" name="tahun_lahir_wali" id="date"> @error('date')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Ayah</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_ayah" id="pend" placeholder="Masukkan Jenjang Pendidikan Ayah"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Ibu</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_ibu" id="pend" placeholder="Masukkan Jenjang Pendidikan Ibu"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="pend">Jenjang Pendidikan Wali</label>
     <input class="form-control @error('pend') is-invalid @enderror" type="text" name="jenjang_pendidikan_wali" id="pend" placeholder="Masukkan Jenjang Pendidikan Wali"> @error('pend')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Ayah</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_ayah" id="work" placeholder="Masukkan Pekerjaan Ayah"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Ibu</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_ibu" id="work" placeholder="Masukkan Pekerjaan Ibu"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="work">Pekerjaan Wali</label>
     <input class="form-control @error('work') is-invalid @enderror" type="text" name="pekerjaan_wali" id="work" placeholder="Masukkan Pekerjaan Wali"> @error('work')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Ayah</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_ayah" id="salary" placeholder="Masukkan Penghasilan Ayah"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Ibu</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_ibu" id="salary" placeholder="Masukkan Penghasilan Ibu"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="salary">Penghasilan Wali</label>
     <input class="form-control @error('salary') is-invalid @enderror" type="text" name="penghasilan_wali" id="salary" placeholder="Masukkan Penghasilan Wali"> @error('salary')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

  <div class="form-group">
    <label for="nik">NIK Ayah</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="text" name="nik_ayah" id="nik" placeholder="Masukkan NIK Ayah"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK Ibu</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="text" name="nik_ibu" id="nik" placeholder="Masukkan NIK Ibu"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="nik">NIK Wali</label>
    <input class="form-control @error('nik') is-invalid @enderror" type="text" name="nik_wali" id="nik_" placeholder="Masukkan NIK Wali"> @error('nik')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="account">ID Account</label>
    <input class="form-control @error('account') is-invalid @enderror" type="number" name="id_account" id="account" placeholder="Masukkan id account"> @error('account')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

    <div class="container-contact2-form-btn">
      <div class="wrap-contact2-form-btn">
        <div class="contact2-form-bgbtn"></div>
        <button class="btn btn-success btn-flat">
          Simpan Data
        </button>
      </div>
    </div>
  </form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection