@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Data Bimbingan')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/bimbingan">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/bimbingan/store" method="post">
    {{ csrf_field() }}
        <!-- <div class="form-group">
      <label for="id">ID Bimbingan</label>
      <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_bimbingan" id="id" placeholder="Masukkan ID Bimbingan"> @error('id')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div> -->

    <div class="form-group">
      <label for="date">Tanggal Bimbingan</label>
      <input class="form-control @error('date') is-invalid @enderror" type="date" name="tanggal_bimbingan" id="date"> @error('date')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
     <label for="desc">Permasalahan</label>
     <input class="form-control @error('desc') is-invalid @enderror" type="text" name="permasalahan" id="desc" placeholder="Masukkan Permasalahan"> @error('desc')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
     <label for="tindakan">Solusi</label>
     <input class="form-control @error('tindakan') is-invalid @enderror" type="text" name="solusi" id="tindakan" placeholder="Masukkan Solusi"> @error('tindakan')
     <div class="invalid-feedback">{{ $message }}</div>
     @enderror
   </div>

   <div class="form-group">
    <label for="id">ID Siswa</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_siswa" id="id" placeholder="Masukkan id siswa"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="id">ID Guru BK</label>
    <input class="form-control @error('id') is-invalid @enderror" type="number" name="id_guruBK" id="id" placeholder="Masukkan id Guru BK"> @error('id')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="container-contact2-form-btn">
    <div class="wrap-contact2-form-btn">
      <div class="contact2-form-bgbtn"></div>
      <button class="btn btn-success btn-flat">
        Simpan Data
      </button>
    </div>
  </div>
</form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
@endsection