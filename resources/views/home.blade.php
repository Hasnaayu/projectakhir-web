@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Home')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">
      @if(auth()->user())
      Hello {{ auth()->user()->name }}, 
      <br><br>Welcome to BiKos Admin Page...</h3>
      @else
      <h3>Welcome to BiKos Admin Page.</h3>
      @endif
      <div class="box-tools pull-right">
      </div>
    </div>
  </div>
  <!-- /.box -->
  @endsection
