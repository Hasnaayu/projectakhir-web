@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Data Akun Siswa')

@section('content')
    <!-- Default box -->
    <div class="box-header">
        <a class="btn btn-success btn-flat" href="/akunsiswa/tambahakunsiswa"> + Tambah Akun Siswa</a>
    </div>
    <div class="box-body">
        <table border="1">
            <tr>
               <!--  <th>ID Akun</th> -->
                <th>E-Mail</th>
                <th>Username</th>
                <th>Password</th>
                <th>ID Role</th>
                <!-- <th>Opsi</th> -->
            </tr>
            @foreach($siswa as $s)
                <tr>
                    <!-- <td>{{ $s->id }}</td> -->
                    <td>{{ $s->email }}</td>
                    <td>{{ $s->name }}</td>
                    <td>{{ $s->password }}</td>
                    <td>{{ $s->id_role }}</td>
                    
            <span class="input-group-btn">
              <!-- <a class="btn btn-warning btn-flat" href="/akunsiswa/editakunsiswa/{{ $s->id }}">Edit</a> -->
              <!--<a class="btn btn-danger btn-flat" href="/akunsiswa/hapusakunsiswa/{{ $s->id }}">Hapus</a>-->
            </span>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
