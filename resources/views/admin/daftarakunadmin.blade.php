@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Data Akun Admin')

@section('content')
    <!-- Default box -->
    <div class="box-header">
        <a class="btn btn-success btn-flat" href="/akunadmin/tambahakunadmin"> + Tambah Akun Admin</a>
    </div>
    <div class="box-body">
        <table border="1">
            <tr>
                <!-- <th>ID Akun</th> -->
                <th>E-Mail</th>
                <th>Username</th>
                <th>Password</th>
                <th>ID Role</th>
                <!-- <th>Opsi</th> -->
            </tr>
            @foreach($admin as $a)
                <tr>
                    <!-- <td>{{ $a->id }}</td> -->
                    <td>{{ $a->email }}</td>
                    <td>{{ $a->name }}</td>
                    <td>{{ $a->password }}</td>
                    <td>{{ $a->id_role }}</td>
                    
            <span class="input-group-btn">
              <!--<a class="btn btn-warning btn-flat" href="/akunadmin/editakunadmin/{{ $a->id }}">Edit</a>
              <a class="btn btn-danger btn-flat" href="/akunadmin/hapusakunadmin/{{ $a->id }}">Hapus</a>-->
            </span>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
