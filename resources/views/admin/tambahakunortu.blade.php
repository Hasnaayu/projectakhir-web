@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Tambah Akun Orang Tua')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header">
    <a class="btn btn-success btn-flat" href="/daftar/akun/ortu">Kembali</a>
  </div>
  <div class="box-body">
   <form action="/daftar/akun/ortu/storeortu" method="post">
    {{ csrf_field() }}
        <!-- <div class="wrap-input2">
          <input class="input2" type="text" name="id" required="required">
          <span class="focus-input2" data-placeholder="ID Account"></span>
        </div> -->

        <div class="form-group">
          <label for="email">Email</label>
          <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" placeholder="Masukkan Email"> @error('email')
          <div class="invalid-feedback">{{ $message }}</div>
          @enderror
        </div>

        <div class="form-group">
          <label for="name">Username</label>
          <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" placeholder="Masukkan Username"> @error('name')
          <div class="invalid-feedback">{{ $message }}</div>
          @enderror
        </div>

        <div class="form-group">
          <label for="password">Password</label>
          <input class="form-control @error('password') is-invalid @enderror" type="text" name="password" id="password" placeholder="Masukkan Password"> @error('password')
          <div class="invalid-feedback">{{ $message }}</div>
          @enderror
        </div>

        <div class="container-contact2-form-btn">
          <div class="wrap-contact2-form-btn">
            <div class="contact2-form-bgbtn"></div>
            <button class="btn btn-success btn-flat">
              Simpan Data
            </button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection