@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Data Akun Guru BK')

@section('content')
    <!-- Default box -->
    <div class="box-header">
        <a class="btn btn-success btn-flat" href="/akungurubk/tambahakungurubk"> + Tambah Akun Guru BK</a>
    </div>
    <div class="box-body">
        <table border="1">
            <tr>
                <!-- <th>ID Akun</th> -->
                <th>E-Mail</th>
                <th>Username</th>
                <th>Password</th>
                <th>ID Role</th>
                <!-- <th>Opsi</th> -->
            </tr>
            @foreach($gurubk as $g)
                <tr>
                    <!-- <td>{{ $g->id }}</td> -->
                    <td>{{ $g->email }}</td>
                    <td>{{ $g->name }}</td>
                    <td>{{ $g->password }}</td>
                    <td>{{ $g->id_role }}</td>
                    
            <span class="input-group-btn">
              <!-- <a class="btn btn-warning btn-flat" href="/akungurubk/editakungurubk/{{ $g->id }}">Edit</a> -->
              <!--<a class="btn btn-danger btn-flat" href="/akungurubk/hapusakungurubk/{{ $g->id }}">Hapus</a>-->
            </span>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
