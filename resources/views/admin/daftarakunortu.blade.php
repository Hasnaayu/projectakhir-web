@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-title','Data Akun Orang Tua')

@section('content')
    <!-- Default box -->
    <div class="box-header">
        <a class="btn btn-success btn-flat" href="/akunortu/tambahakunortu"> + Tambah Akun Orang Tua</a>
    </div>
    <div class="box-body">
        <table border="1">
            <tr>
                <!-- <th>ID Akun</th> -->
                <th>E-Mail</th>
                <th>Username</th>
                <th>Password</th>
                <th>ID Role</th>
               <!--  <th>Opsi</th> -->
            </tr>
            @foreach($ortu as $o)
                <tr>
                    <!-- <td>{{ $o->id }}</td> -->
                    <td>{{ $o->email }}</td>
                    <td>{{ $o->name }}</td>
                    <td>{{ $o->password }}</td>
                    <td>{{ $o->id_role }}</td>
                    
            <span class="input-group-btn">
              <!-- <a class="btn btn-warning btn-flat" href="/akunortu/editakunortu/{{ $o->id }}">Edit</a> -->
              <!--<a class="btn btn-danger btn-flat" href="/akunortu/hapusakunortu/{{ $o->id }}">Hapus</a>-->
            </span>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
