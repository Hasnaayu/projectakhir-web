@extends('layouts.app')
  @section('title', 'Dashboard')
  @section('page-title','Edit Akun Guru BK')

  @section('content')
  <!-- Default box -->
  <div class="box">
  	<div class="box-header">
  		<a class="btn btn-success btn-flat" href="/daftar/akun/gurubk">Kembali</a>
  	</div>
    <div class="box-body">
		@foreach($gurubk as $g)
		<form action="/daftar/akun/gurubk/updategurubk" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $g->id }}">
			
		    <div class="wrap-input2">
          <input class="input2" type="text" name="email" required="required">
          <span class="focus-input2" data-placeholder="Email"></span>
        </div>

        <div class="wrap-input2">
          <input class="input2" type="text" name="name" required="required">
          <span class="focus-input2" data-placeholder="Username"></span>
        </div>

        <div class="wrap-input2">
          <input class="input2" type="text" name="password" required="required">
          <span class="focus-input2" data-placeholder="Password"></span>
        </div>

         <div class="wrap-input2">
          <input class="input2" type="text" name="id_role" required="required">
          <span class="focus-input2" data-placeholder="ID Role"></span>
        </div>

		    <div class="container-contact2-form-btn">
		      <div class="wrap-contact2-form-btn">
		        <button class="btn btn-success btn-flat">
		          Simpan Data
		        </button>
		      </div>
		    </div>
		</form>
		@endforeach
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @endsection