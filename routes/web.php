<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AdminAccessOnlyMiddleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('login');
});

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::middleware(AdminAccessOnlyMiddleware::class)->group(function () {

//route CRUD Data Siswa
	Route::get('/siswa', 'App\Http\Controllers\SiswaController@index');
	Route::get('/siswa/tambahsiswa','App\Http\Controllers\SiswaController@tambahsiswa');
	Route::post('/siswa/store','App\Http\Controllers\SiswaController@store');
	Route::get('/siswa/editsiswa/{id_siswa}','App\Http\Controllers\SiswaController@editsiswa');
	Route::post('/siswa/update','App\Http\Controllers\SiswaController@update');
	Route::get('/siswa/hapussiswa/{id_siswa}','App\Http\Controllers\SiswaController@hapussiswa');

//route CRUD Data Guru BK
	Route::get('/gurubk', 'App\Http\Controllers\GurubkController@index');
	Route::get('/gurubk/tambahgurubk','App\Http\Controllers\GurubkController@tambahgurubk');
	Route::post('/gurubk/store','App\Http\Controllers\GurubkController@store');
	Route::get('/gurubk/editgurubk/{id_gurubk}','App\Http\Controllers\GurubkController@editgurubk');
	Route::post('/gurubk/update','App\Http\Controllers\GurubkController@update');
	Route::get('/gurubk/hapusgurubk/{id_gurubk}','App\Http\Controllers\GurubkController@hapusgurubk');

//route CRUD Data Orang Tua
	Route::get('/ortu', 'App\Http\Controllers\OrtuController@index');
	Route::get('/ortu/tambahortu','App\Http\Controllers\OrtuController@tambahortu');
	Route::post('/ortu/store','App\Http\Controllers\OrtuController@store');
	Route::get('/ortu/editortu/{id_ortu}','App\Http\Controllers\OrtuController@editortu');
	Route::post('/ortu/update','App\Http\Controllers\OrtuController@update');
	Route::get('/ortu/hapusortu/{id_ortu}','App\Http\Controllers\OrtuController@hapusortu');

//route CRUD Data Admin
	Route::get('/admin', 'App\Http\Controllers\AdmindataController@index');
	Route::get('/admin/tambahadmin','App\Http\Controllers\AdmindataController@tambahadmin');
	Route::post('/admin/store','App\Http\Controllers\AdmindataController@store');
	Route::get('/admin/editadmin/{id_admin}','App\Http\Controllers\AdmindataController@editadmin');
	Route::post('/admin/update','App\Http\Controllers\AdmindataController@update');
	Route::get('/admin/hapusadmin/{id_admin}','App\Http\Controllers\AdmindataController@hapusadmin');

//route CRUD Data Pelanggaran
	Route::get('/pelanggaran', 'App\Http\Controllers\PelanggaranController@index');
	Route::get('/pelanggaran/tambahpelanggaran','App\Http\Controllers\PelanggaranController@tambahpelanggaran');
	Route::post('/pelanggaran/store','App\Http\Controllers\PelanggaranController@store');
	Route::get('/pelanggaran/editpelanggaran/{id_pelanggaran}','App\Http\Controllers\PelanggaranController@editpelanggaran');
	Route::post('/pelanggaran/update','App\Http\Controllers\PelanggaranController@update');
	Route::get('/pelanggaran/hapuspelanggaran/{id_pelanggaran}','App\Http\Controllers\PelanggaranController@hapuspelanggaran');

//route CRUD Data Bimbingan
	Route::get('/bimbingan', 'App\Http\Controllers\BimbinganController@index');
	Route::get('/bimbingan/tambahbimbingan','App\Http\Controllers\BimbinganController@tambahbimbingan');
	Route::post('/bimbingan/store','App\Http\Controllers\BimbinganController@store');
	Route::get('/bimbingan/editbimbingan/{id_bimbingan}','App\Http\Controllers\BimbinganController@editbimbingan');
	Route::post('/bimbingan/update','App\Http\Controllers\BimbinganController@update');
	Route::get('/bimbingan/hapusbimbingan/{id_bimbingan}','App\Http\Controllers\BimbinganController@hapusbimbingan');

//Daftar Akun Admin
	Route::get('/daftar/akun/admin', 'App\Http\Controllers\AdminController@akunadmin');
	Route::get('/akunadmin/tambahakunadmin','App\Http\Controllers\AdminController@tambahakunadmin');
	Route::post('/daftar/akun/admin/storeadmin','App\Http\Controllers\AdminController@storeadmin');
	Route::get('/akunadmin/editakunadmin/{id}','App\Http\Controllers\AdminController@editakunadmin');
	Route::post('/daftar/akun/admin/updateadmin','App\Http\Controllers\AdminController@updateadmin');

//Daftar Akun Siswa
	Route::get('/daftar/akun/siswa', 'App\Http\Controllers\AdminController@akunsiswa');
	Route::get('/akunsiswa/tambahakunsiswa','App\Http\Controllers\AdminController@tambahakunsiswa');
	Route::post('/daftar/akun/siswa/storesiswa','App\Http\Controllers\AdminController@storesiswa');
	Route::get('/akunsiswa/editakunsiswa/{id}','App\Http\Controllers\AdminController@editakunsiswa');
	Route::post('/daftar/akun/siswa/updatesiswa','App\Http\Controllers\AdminController@updatesiswa');

//Daftar Akun GuruBK
	Route::get('/daftar/akun/gurubk', 'App\Http\Controllers\AdminController@akungurubk');
	Route::get('/akungurubk/tambahakungurubk','App\Http\Controllers\AdminController@tambahakungurubk');
	Route::post('/daftar/akun/gurubk/storegurubk','App\Http\Controllers\AdminController@storegurubk');
	Route::get('/akungurubk/editakungurubk/{id}','App\Http\Controllers\AdminController@editakungurubk');
	Route::post('/daftar/akun/gurubk/updategurubk','App\Http\Controllers\AdminController@updategurubk');

//Daftar Akun Ortu
	Route::get('/daftar/akun/ortu', 'App\Http\Controllers\AdminController@akunortu');
	Route::get('/akunortu/tambahakunortu','App\Http\Controllers\AdminController@tambahakunortu');
	Route::post('/daftar/akun/ortu/storeortu','App\Http\Controllers\AdminController@storeortu');
	Route::get('/akunortu/editakunortu/{id}','App\Http\Controllers\AdminController@editakunortu');
	Route::post('/daftar/akun/ortu/updateortu','App\Http\Controllers\AdminController@updateortu');

//route CRUD Data Kelas
	Route::get('/kelas', 'App\Http\Controllers\KelasController@indexkelas');
	Route::get('/kelas/tambahkelas','App\Http\Controllers\KelasController@tambahkelas');
	Route::post('/kelas/store','App\Http\Controllers\KelasController@store');
	Route::get('/kelas/editkelas/{id_ortu}','App\Http\Controllers\KelasController@editkelas');
	Route::post('/kelas/update','App\Http\Controllers\KelasController@update');
	Route::get('/kelas/hapuskelas/{id_ortu}','App\Http\Controllers\KelasController@hapuskelas');

//route Grafik Kelas
	Route::get('/pelanggaran/grafikpelanggarankelas', 'App\Http\Controllers\PelanggaranController@IndexGrafikPelanggaranKelas');

	Route::get('/pelanggaran/grafikpelanggaransiswa', 'App\Http\Controllers\PelanggaranController@GrafikPelanggaranSiswa');

//import
	Route::post('/users/import', 'App\Http\Controllers\UsersImportController@store');
	Route::post('/siswaimports', 'App\Http\Controllers\SiswaController@siswaimports');
	Route::post('/gurubkimports', 'App\Http\Controllers\GurubkController@gurubkimports');
	Route::post('/ortuimports', 'App\Http\Controllers\OrtuController@ortuimports');
	Route::post('/pelanggaranimports', 'App\Http\Controllers\PelanggaranController@pelanggaranimports');

//export
	Route::get('/siswaexports', 'App\Http\Controllers\SiswaController@siswaexports');
	Route::get('/gurubkexports', 'App\Http\Controllers\GurubkController@gurubkexports');
	Route::get('/ortuexports', 'App\Http\Controllers\OrtuController@ortuexports');
	Route::get('/pelanggaranexports', 'App\Http\Controllers\PelanggaranController@pelanggaranexports');


});

