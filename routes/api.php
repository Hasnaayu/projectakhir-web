<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

//Route::group(['prefix' => 'v1'], function () {
Route::post('/login', 'App\Http\Controllers\UsersController@login');
Route::get('/logout', 'App\Http\Controllers\UsersController@logout')->middleware('auth:api');
Route::get('/aum/indexaum', 'App\Http\Controllers\AumController@indexaum')->middleware('auth:api');
Route::get('/aum/store', 'App\Http\Controllers\AumController@store')->middleware('auth:api');
//});

//password
Route::post('user/change_password', 'App\Http\Controllers\ApiChangePasswordController@change');
Route::post('user/reset_password', 'App\Http\Controllers\ApiChangePasswordController@reset');

//Siswa
Route::get('/siswa', 'App\Http\Controllers\SiswaController@indexuser')->middleware('auth:api');
Route::post('/siswa/storesiswa', 'App\Http\Controllers\SiswaController@storesiswa')->middleware('auth:api');
Route::get('/siswa/indexnama', 'App\Http\Controllers\SiswaController@indexnama')->middleware('auth:api');
Route::get('/siswa/kelas/{id_kelas}', 'App\Http\Controllers\SiswaController@siswaperkelas');

//GuruBK
Route::get('/gurubk', 'App\Http\Controllers\GurubkController@indexgurubk')->middleware('auth:api');
Route::post('/siswa/storegurubk', 'App\Http\Controllers\GurubkController@storegurubk')->middleware('auth:api');

//Pelanggaran
Route::get('/pelanggaransiswa', 'App\Http\Controllers\PelanggaranController@pelanggaransiswa')->middleware('auth:api');
Route::get('/pelanggaranortu', 'App\Http\Controllers\PelanggaranController@pelanggaranortu')->middleware('auth:api');
Route::get('/pelanggaran/{id}', 'App\Http\Controllers\PelanggaranController@indexpelanggaran')->middleware('auth:api');
Route::get('/pelanggaran/grafik/siswa/{id_siswa}/{tahun}', 'App\Http\Controllers\PelanggaranController@grafikPelanggaranPerSiswa');
Route::get('/pelanggaran/grafik/{id}/{tahun}', 'App\Http\Controllers\PelanggaranController@grafikPelanggaranKelas');
Route::post('/pelanggaran/storepelanggaran', 'App\Http\Controllers\PelanggaranController@storepelanggaran')->middleware('auth:api');
Route::delete('/pelanggaran/deletepelanggaran/data/{id}', 'App\Http\Controllers\PelanggaranController@deletepelanggaran')->middleware('auth:api');
Route::put('/pelanggaran/updatepelanggaran/{id}','App\Http\Controllers\PelanggaranController@updatepelanggaran')->middleware('auth:api');
Route::get('/pelanggaran/ortu/grafik/siswa/{tahun}', 'App\Http\Controllers\PelanggaranController@grafikPelanggaranForOrtu')->middleware('auth:api');

//Kelas
Route::get('/kelas/listkelas', 'App\Http\Controllers\KelasController@listkelas')->middleware('auth:api');
Route::resource('/kelas', 'App\Http\Controllers\KelasController')->middleware('auth:api');

//aum
Route::post('/aumform/storeform', 'App\Http\Controllers\AumformController@storeform')->middleware('auth:api');
Route::post('/storeaum', 'App\Http\Controllers\AumController@storeaum')->middleware('auth:api');
Route::get('/getaum/{id}', 'App\Http\Controllers\AumController@getaum')->middleware('auth:api');
Route::get('/getaumform/{id}', 'App\Http\Controllers\AumformController@getaumform')->middleware('auth:api');

//bimbingan
Route::get('/bimbingan/{id}', 'App\Http\Controllers\BimbinganController@indexbimbingan')->middleware('auth:api');
Route::post('/bimbingan/storebimbingan', 'App\Http\Controllers\BimbinganController@storebimbingan')->middleware('auth:api');
Route::delete('/bimbingan/deletebimbingan/data/{id}', 'App\Http\Controllers\BimbinganController@deletebimbingan')->middleware('auth:api');
Route::put('/bimbingan/updatebimbingan/{id}','App\Http\Controllers\BimbinganController@updatebimbingan')->middleware('auth:api');
Route::get('/bimbingansiswa', 'App\Http\Controllers\BimbinganController@bimbingansiswa')->middleware('auth:api');

//tatib
Route::get('/tatibsatu', 'App\Http\Controllers\TatibController@tatibsatu')->middleware('auth:api');
Route::get('/tatibdua', 'App\Http\Controllers\TatibController@tatibdua')->middleware('auth:api');
Route::get('/tatibtiga', 'App\Http\Controllers\TatibController@tatibtiga')->middleware('auth:api');
Route::get('/tatibempat', 'App\Http\Controllers\TatibController@tatibempat')->middleware('auth:api');
Route::get('/tatiblima', 'App\Http\Controllers\TatibController@tatiblima')->middleware('auth:api');
Route::get('/tatibenam', 'App\Http\Controllers\TatibController@tatibenam')->middleware('auth:api');

//ortu
Route::get('/indexortu', 'App\Http\Controllers\OrtuController@indexortu')->middleware('auth:api');

//bot
Route::get('/tanyabot', 'App\Http\Controllers\PertanyaanbotController@tanyabot')->middleware('auth:api');
Route::post('/postsesibot', 'App\Http\Controllers\PertanyaanbotController@storesesi')->middleware('auth:api');
Route::post('/storeresponse', 'App\Http\Controllers\ChatbotController@storeresponse')->middleware('auth:api');
Route::get('/responsesiswa/{id}', 'App\Http\Controllers\ChatbotController@responsesiswa')->middleware('auth:api');
Route::get('/listpertanyaan', 'App\Http\Controllers\ChatbotController@listpertanyaan')->middleware('auth:api');
Route::get('/jawabanbot/{id}', 'App\Http\Controllers\ChatbotController@jawabanbot')->middleware('auth:api');
Route::post('/postbot/{id}', 'App\Http\Controllers\ChatbotController@postbot')->middleware('auth:api');
Route::get('/tanyajawab', 'App\Http\Controllers\ChatbotController@tanyajawab')->middleware('auth:api');
Route::get('/jawabbot', 'App\Http\Controllers\ChatbotController@jawabbot')->middleware('auth:api');
Route::get('/topikbot', 'App\Http\Controllers\ChatbotController@topik')->middleware('auth:api');
Route::get('/topikbot/{id}', 'App\Http\Controllers\ChatbotController@topikbot')->middleware('auth:api');

//jadwal
Route::post('/storejadwalsiswa', 'App\Http\Controllers\JadwalController@createJadwal')->middleware('auth:api');
Route::get('/jadwal/index', 'App\Http\Controllers\JadwalController@index')->middleware('auth:api');
Route::put('/add_to_confirm/{id}', 'App\Http\Controllers\JadwalController@add_to_confirm')->middleware('auth:api');
Route::put('/add_to_fail/{id}', 'App\Http\Controllers\JadwalController@add_to_fail')->middleware('auth:api');
Route::put('/updatejadwal/{id}','App\Http\Controllers\JadwalController@update')->middleware('auth:api');
Route::delete('/deletejadwal/{id}', 'App\Http\Controllers\JadwalController@delete')->middleware('auth:api');
Route::get('/jadwalguru', 'App\Http\Controllers\JadwalController@jadwalguru')->middleware('auth:api');

//chat
Route::get('/chats', 'App\Http\Controllers\ChatController@index')->middleware('auth:api');
Route::post('/messages', 'App\Http\Controllers\MessageController@store')->middleware('auth:api');
Route::post('/chats', 'App\Http\Controllers\ChatController@store')->middleware('auth:api');
Route::post('/chats/read','App\Http\Controllers\ChatController@makeConversationAsReaded')->middleware('auth:api');
Route::post('/fcm','App\Http\Controllers\UsersController@fcmToken')->middleware('auth:api');

//USER SECURITY PIN
Route::get('/user_security_pin/index', 'App\Http\Controllers\UserSecurityPinController@index')->middleware('auth:api');
Route::post('/user_security_pin/store', 'App\Http\Controllers\UserSecurityPinController@store')->middleware('auth:api');
Route::put('/user_security_pin/update/{id}', 'App\Http\Controllers\UserSecurityPinController@update')->middleware('auth:api');
Route::delete('/user_security_pin/delete/{id}', 'App\Http\Controllers\UserSecurityPinController@delete')->middleware('auth:api');

//panggilanortu
Route::post('/storepanggilan', 'App\Http\Controllers\PanggilanOrtuController@createPanggilan')->middleware('auth:api');
Route::get('/panggilanortu', 'App\Http\Controllers\PanggilanOrtuController@panggilanortu')->middleware('auth:api');
Route::put('/add_to_done/{id}', 'App\Http\Controllers\PanggilanOrtuController@add_to_done')->middleware('auth:api');
Route::put('/add_to_fail/{id}', 'App\Http\Controllers\PanggilanOrtuController@add_to_fail')->middleware('auth:api');
Route::put('/updatepanggilan/{id}','App\Http\Controllers\PanggilanOrtuController@update')->middleware('auth:api');
Route::delete('/deletepanggilan/{id}', 'App\Http\Controllers\PanggilanOrtuController@delete')->middleware('auth:api');
Route::get('/panggilan/{id}', 'App\Http\Controllers\PanggilanOrtuController@indexpanggilan')->middleware('auth:api');



